-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3308
-- Thời gian đã tạo: Th9 17, 2020 lúc 07:24 AM
-- Phiên bản máy phục vụ: 5.7.28
-- Phiên bản PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bannhac_new`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  KEY `item_name` (`item_name`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `type` (`type`),
  KEY `rule_name` (`rule_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `parent` (`parent`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1598678917);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `song`
--

DROP TABLE IF EXISTS `song`;
CREATE TABLE IF NOT EXISTS `song` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lyric` text COLLATE utf8_unicode_ci NOT NULL,
  `key_chord` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `first_lyric` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chorus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `tempo` int(11) DEFAULT NULL,
  `link_song` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `song`
--

INSERT INTO `song` (`id`, `title`, `slug`, `lyric`, `key_chord`, `type_id`, `first_lyric`, `chorus`, `topic_id`, `tempo`, `link_song`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(10, 'Ngài là Giê-xu quyền bính trong tay Ngài', 'ngai-la-gie-xu-quyen-binh-trong-tay-ngai', '<p>[1]<br />\r\n[Am]Ngài là Giê-xu, [F]quyền bính trong [Am]tay Ngài.[F]<br />\r\nNgài đã sống [C]lại, chiến thắng cõi [E]chết.<br />\r\n[chorus]<br />\r\nChúa đã sống [Am]lại!(x2)[  C] [D]</p>\r\n\r\n<p>Ha-lê-lu-[  F]gia!<br />\r\nChúa đã sống [Am]lại! Đưa tay cao [F]lên tôn thờ [Am]Chúa<br />\r\nVỗ tay cho [E]đều ngợi khen Chúa [Am]ta.</p>\r\n', 'Am', 3, 'Ngài là Giê-xu, quyền bính trong tay Ngài.', 'Chúa đã sống lại! Chúa đã sống lại!', 1, 120, '', '2020-08-24 14:29:43', 1, '2020-08-29 03:01:51', 1),
(11, 'Chúa Đấng Chiến Thắng', 'chua-dang-chien-thang', '<p>[1]</p>\r\n\r\n<p>Giê-[Am]xu là Vua các [G]vua!</p>\r\n\r\n<p>Giê-[E]xu là Chúa muôn [Am]vua! (x2)</p>\r\n\r\n<p>[F]Các vua trần gian sẽ quỳ [G]lạy trước ngôi [Am]Ngài.</p>\r\n\r\n<p>[F]Các vua trần gian sẽ quỳ [G]lạy trước Danh Giê-[Am]xu.</p>\r\n\r\n<p>[F]Các cửa âm phủ sẽ không đứng [G]nổi trước Danh Giê-[Am]xu.</p>\r\n\r\n<p>[F]Các cửa âm phủ sẽ không đứng [G]nổi trước Hội Thánh [Am]Ngài.</p>\r\n\r\n<p>[chorus]</p>\r\n\r\n<p>Anh [F]ơi! Hãy vững [G]lòng vì Chúa [E]thắng thế gian [Am]rồi.</p>\r\n\r\n<p>Anh [F]ơi! Hãy vững [G]lòng vì Chúa [E]chiến thắng ma [Am]vương.</p>\r\n', 'Am', 3, 'Giê-xu là Vua các vua!  Giê-xu là Chúa muôn vua!', 'Anh ơi! Hãy vững lòng vì Chúa thắng thế gian rồi. ', NULL, 120, '', '2020-08-26 15:32:16', 1, '2020-08-26 16:37:48', 1),
(12, 'Giờ này lòng con hướng về Ngài', 'gio-nay-long-con-huong-ve-ngai', '<p>[1]<br />\r\n[C]Giờ này lòng [D]con hướng về [G]Ngài. [C]Tụng ngợi Giê-[D]xu Vua oai [G]nghi.<br />\r\n[C]Mọi lời chúc [D]tán tôn vinh, [Bm]Duy thuộc về Ngài [Em]thôi. [C]Quỳ nơi đây trọn [Am]lòng con đây tôn [D]vinh. (x2)<br />\r\n[chorus]<br />\r\n[C]Cùng nhau đưa cao [D]đôi tay, và [Bm]cất tiếng hát vang [Em]đến ngôi Ngài. [C]Tại nơi đây [D]Chúa được vinh [G]hiển.<br />\r\n[C]Cùng nhau đưa [D]cao đôi tay, và [Bm]cất tiếng hát vang [Em]đến ngôi Ngài. [C]Tại nơi đây [D]Chúa được tôn [G]cao.</p>\r\n', 'G', 3, 'Giờ này lòng con hướng về Ngài, tụng ngợi Giê-xu', 'Cùng nhau đưa cao đôi tay, và cất tiếng hát', NULL, 90, '', '2020-08-29 01:31:49', 1, '2020-08-29 01:35:28', 1),
(13, 'Hãy cất tiếng hát ngợi khen Chúa', 'hay-cat-tieng-hat-ngoi-khen-chua', '<p>[1]<br />\r\n[C]Hãy cất tiếng hát ngợi khen Chúa! [G]Giê-hô-va Nissi, Nissi! <br />\r\n[F]Chúa của mọi đắc thắng vinh quang, Ngài ban cho [G]ta chiến thắng thế gian! <br />\r\n[C]Nay ta luôn đắc thắng nghèo đói. [G]Nay ta luôn đắc thắng bệnh tật. <br />\r\n[F]Nay ta luôn đắc thắng buồn chán, từ nay chiến [G]thắng thuộc về [C]ta. <br />\r\n[chorus]<br />\r\n[F]Amen! [G]Amen! [Am]Amen! [F]Lớn tiếng hát khen Danh [C]Ngài. <br />\r\n[F]Trong Danh [G]Cha ta [Am]đắc thắng (đắc thắng). [F]Sự chiến thắng thuộc về [C]ta.</p>\r\n', 'C', 3, 'Hãy cất tiếng hát ngợi khen Chúa, Giê-hô-va Nissi Nissi', 'A-men A-men A-men, lớn tiếng hát khen danh Ngài', NULL, 120, '', '2020-08-29 01:39:15', 1, '2020-08-29 01:39:23', 1),
(14, 'Múa, hát, hãy vỗ tay reo vui', 'mua-hat-hay-vo-tay-reo-vui', '<p>[1]<br />\r\n[D]Múa, [A]hát, [G]hãy vỗ tay reo vui lên bởi chúng [D]ta tự [A]do [D]khỏi đau khổ lo [A]sợ.<br />\r\n[D]Múa, [A]hát, [G]hãy vỗ tay reo vui lên bởi chúng [D]ta tự [A]do, khỏi bao khổ [D]đau.[D7]    <br />\r\n[chorus]<br />\r\n[G]Giê[A]-[ D]xu, [G]Giê[A]-[ D]xu ! Sự [G]tôn vinh Cha luôn [A]vẫn đến cả trong [F#m]nơi tận cõi vĩnh [Bm]cửu sẽ [G]thấy ở [A]trong môi miệng con [D]đây.[D7]<br />\r\nSự [G]tôn vinh Cha luôn [A]vẫn đến cả trong [F#m]nơi tận cõi vĩnh [Bm]cửu sẽ [G]thấy ở [A]trong môi miệng này [D]đây.</p>\r\n', 'D', 3, 'Múa, hát, hãy vỗ tay reo vui lên bởi chúng ta', 'Giê-xu, Giê-xu! sự tôn vinh Cha luôn vẫn đến cả', NULL, 120, '', '2020-08-29 02:56:36', 1, '2020-08-29 02:58:36', 1),
(15, 'Chúc Tôn Chúa', 'chuc-ton-chua', '<p>[1]<br />\r\n[Dm]Hãy cùng mọi người tôn kính [C]Cha oai quyền<br />\r\nVinh [Bb]hiển Ngài rạng ngời khắp thế [A7]gian<br />\r\n[Dm]Hết lòng thờ phượng, [C]hát ca tụng Ngài<br />\r\nTôn [Bb]Cha là Chúa Trời vinh [A7]hiển<br />\r\n[chorus]<br />\r\n[Dm]Chúa vinh hiển! [Dm7/C]Chúa vinh hiển<br />\r\n[Dm7/Bb]Hát chúc tôn danh Chúa<br />\r\n[A7]Hát chúc tôn danh Chúa<br />\r\n[Dm]Chúa vinh hiển! [Dm7/C]Chúa vinh hiển<br />\r\n[Dm7/Bb]Hát chúc tôn danh Chúa<br />\r\n[A7]Hát chúc tôn danh Chúa<br />\r\n[Dm]Ha-lê-lu-gia<br />\r\n[Dm7/C]Khắp nơi tôn vinh Cha<br />\r\n[Bb]Chúa hiển [C]vinh quyền [Dm]oai</p>\r\n', 'Dm', 3, 'Hãy cùng mọi người tôn kính Cha oai quyền', 'Chúa vinh hiển, Chúa vinh hiển, hát chúc tôn danh Chúa', NULL, 120, '', '2020-08-29 03:12:30', 1, '2020-08-29 03:17:03', 1),
(16, 'Đoàn quân đắc thắng', 'doan-quan-dac-thang', '<p>[1]<br />\r\n[Em]Giê-xu Vua mọi Vua Ngài là [Em]Chúa oai nghi quyền năng,<br />\r\nNào ta [G]hãy mau đến chúc tán thờ tôn, [D]Thánh Danh của [B7]Ngài.<br />\r\n[Em]Giê-xu Vua mọi Vua Danh của C[Em]húa trên hết mọi danh.<br />\r\nNào ta [G]hãy công bố đắc thắng. Trong Danh [D]Giê-xu năng [B7]quyền.<br />\r\n[chorus]<br />\r\n[Em]Trong Danh Vua Giê-xu [Am]Christ. Bóng [D]tối sẽ bị diệt [Em]vong<br />\r\n[Em]Trong Danh Vua Giê-xu [Am]Christ. Chiến [G]thắng sẽ thuộc về [B7]ta.<br />\r\n[Em]Trong Danh Vua Giê-xu [Am]Christ. Chúng [D]ta mãi được tự [Em]do,<br />\r\n[Em]Trong Danh Vua Giê-xu [Am]Christ. Nầy đoàn [D]quân đắc thắng của thiên [B7]quốc.<br />\r\n[bridge]<br />\r\n[Em]Giê-xu Vua mọi Vua<br />\r\n[Ending]<br />\r\n[Em]Giê-xu Vua vinh [B7]hiển</p>\r\n', 'Em', 3, 'Giê-xu Vua mọi vua, Ngài là Chúa oai nghi quyền năng', 'Trong danh Vua Giê-xu Christ, bóng tối sẽ bị diệt vong', NULL, 120, '', '2020-08-29 03:25:20', 1, '2020-08-29 13:52:05', 1),
(17, 'Cảm ơn Cha (Thank You Lord)', 'cam-on-cha-thank-you-lord', '<p>[1]<br />\r\n[G]Vui thay khi con đến với [D]Cha. [Am]Mọi điều từ lòng con chỉ [Em]mong thưa cùng Ngài<br />\r\n[C]Lời biết [D]ơn. [C]Lời cảm [D]ơn<br />\r\n[G]Về mọi điều mà Cha đã [D]ban. [Am]Về mọi nguồn hạnh phước Chúa [Em]tuôn trong cuộc đời<br />\r\n[C]Nguyện biết [D]ơn. [C]Nguyện cảm [D]ơn<br />\r\n[prechorus]<br />\r\n[G]Sẽ nhớ mãi ơn Cha [D]với tiếng chúc tán Chúa <br />\r\n[Em]Hai tay con giơ cao [C]để chúc tán danh Cha<br />\r\n[chorus]<br />\r\n[G]Cảm ơn [D]Ngài!  Biết ơn Cha và [C]cảm ơn [D]Ngài <br />\r\n[G]Cảm ơn [D]Ngài!  Biết ơn Cha và [C]cảm ơn [D]Ngài<br />\r\nSuốt cuộc [G]đời<br />\r\n[2]<br />\r\n[G]Mọi điều Ngài làm cho chính [D]con. [Am]Chiếu ánh sáng thay thế tối [Em]tăm cho cuộc đời<br />\r\n[C]Nguyện biết [D]ơn. [C]Nguyện cảm [D]ơn<br />\r\n[G]Chúa gánh hết tội ô đáng [D]khinh. [Am]Mang bao đau thương và chữa [Em]cho con được lành<br />\r\n[C]Nguyện biết [D]ơn. [C]Nguyện cảm [D]ơn</p>\r\n', 'G', 4, 'Vui thay khi con đến với Cha', 'Cảm ơn Ngài, biết ơn Cha và cảm ơn Ngài', NULL, 100, '', '2020-08-29 06:07:53', 1, '2020-08-29 06:10:13', 1),
(18, 'Chúa hiển vinh', 'chua-hien-vinh', '<p>[1]<br />\r\nBình an [A]Vua, Thần khôn [E]ngoan, đầy ân [D]thiêng Con Chúa [E]Trời.<br />\r\nVua các [A]Chúa, Thần oai [E]nghiêm, ngợi tôn [D]Vua, Cứu [Bm]Chúa của cõi vĩnh [E]cửu. <br />\r\n[Chorus]<br />\r\nCon tôn [A]cao Ngài, Chúa con tôn [E]cao Ngài. Cả [D]tạo vật đang hướng về. <br />\r\nNgài [Bm]Thật vinh hiển duy [E]Chúa. Con tôn cao [A]Ngài, Chúa con tôn [E]cao Ngài.<br />\r\nLà [D]Giê-xu, [E]Chúa con hằng tôn [A]cao.</p>\r\n', 'A', 4, 'Bình an Vua, Thần khôn ngoan, đầy ơn thiêng', 'Con tôn cao Ngài, Chúa con tôn cao Ngài', NULL, 90, '', '2020-08-29 06:14:24', 1, '2020-08-29 06:16:16', 1),
(19, 'Hiến tất cả cho Ngài', 'hien-tat-ca-cho-ngai', '<p>[1]<br />\r\n[A]Xin dâng hết thảy [E]cho Jesus. Tôi<br />\r\n[F#m]Tình nguyện [D]hiến chính [E]thân thể [A]nầy<br />\r\n[A]Luôn yêu mến trông [E]mong nơi Chúa thôi<br />\r\n[F#m]Hằng ngày [D]sống dưới [E]ân điển [A]Ngài.<br />\r\n[chorus]<br />\r\n[A]Hiến tất cả cho [D]Ngài,<br />\r\n[E]Hiến tất cả cho [A]Ngài!<br />\r\n[A]Lạy Jesus! Tôi [F#m]hiến tất [D]cả đây<br />\r\n[E]Hiến tất cả cho [A]Ngài.<br />\r\n[2]<br />\r\n[A]Xin dâng hết thảy [E]cho Jesus. Tôi<br />\r\n[F#m]Nầy đời [D]sống tôi [E]xin hiến [A]Ngài<br />\r\n[A]Xin nhân ái linh [E]năng luôn phước bội<br />\r\n[F#m]từ Ngài [D]đổ xuống [E]lai láng [A]hoài.<br />\r\n[3]<br />\r\n[A]Xin dâng hết thảy [E]cho Jesus. Tôi<br />\r\n[F#m]Lòng nhận [D]biết lửa [E]thiêng cháy [A]hoài<br />\r\n[A]Trong ơn cứu rỗi, [E]ôi vui quá vui<br />\r\n[F#m]Đời đời [D]hiển vinh [E]danh thánh [A]Ngài.</p>\r\n', 'A', 4, 'Xin dâng hết thảy cho Giê-xu tôi tình', 'Hiến tất cả cho Ngài, Hiến tất cả cho Ngài', NULL, 100, '', '2020-08-29 06:28:42', 1, '2020-08-29 06:30:12', 1),
(20, 'Nguyện kính mến tôn thờ Ngài', 'nguyen-kinh-men-ton-tho-ngai', '<p>[1]<br />\r\nTrọn [G]lòng thờ phượng Cha chí [Em]Thánh. <br />\r\nTâm [Am]con nguyện chăm xem Chúa [D]Giê-xu. <br />\r\n[B]Đấng hiển vinh duy Ngài [Em]Vua của [D]chính [C]con,<br />\r\n[Am]Ngài được tôn cao Chúa [D]hỡi.<br />\r\nTrọn [G]lòng thờ phượng Cha thiêng [Em]liêng, <br />\r\nNgài [Am]là niềm vui và sức mới [D]luôn. <br />\r\n[B]Duy Chúa ban hi vọng [Em]và sức [D]mới [C]con.<br />\r\n[Am]Ngài được tôn cao mãi [D]mãi.<br />\r\n[Chorus]<br />\r\nCon [G]tôn [D]cao [Em]Ngài,[E]Giê-xu con [Am]tôn [G]cao [D]Ngài.<br />\r\nMọi [Em]điều trong tâm [D]con và [G]hết linh hồn [C]này.<br />\r\nNguyện [Am]ngợi khen và [D]kính mến tôn thờ [G]Ngài.<br />\r\n[Bridge]<br />\r\nThần Linh [G]tuôn đỗ [Em]đầy nơi đây, ngọt ngào [Am]tươi mới, [D]trên chính con. <br />\r\nNgự nơi [G]đây Chúa [Em]đầy vinh quang. Giờ con [Am]hát chúc tôn, giờ con [D]hát chúc tôn. </p>\r\n', 'G', 4, 'Trọn lòng thờ phượng Cha chí Thánh, tâm con', 'Con tôn cao Ngài, Giê-xu con tôn cao Ngài', NULL, 90, '', '2020-08-29 14:24:56', 1, '2020-08-29 14:29:24', 1),
(21, 'Tôi Không Có Gì Trong Thế Gian Này', 'toi-khong-co-gi-trong-the-gian-nay', '<p>[1]<br />\r\n[C]Tôi không có gì trong thế [F]gian này, mà chỉ [G]có một mình Giê- [C]xu.<br />\r\nKhi cơn [C]gió lùa đi hết [F]mọi điều, Ngài vẫn [G]sống trong tim tôi [Am]hoài. <br />\r\n[C]Tôi không có gì dâng hiến [F]cho Ngài, ngoài bao [G]đau thương và mất [C]mát <br />\r\nTôi gom [C]hết những mảnh vỡ [F]cuộc đời và đặt [G]hết dười chân Giê- [Am]xu.<br />\r\n[Chorus]<br />\r\nTôi chỉ [F]có Giê-xu mà [C]thôi, nhưng trong [F]Ngài tôi nhận [G]được tất [C]cả <br />\r\nMọi châu [F]báu quí giá trần [C]gian này, sao so [F]được Giê-xu Chúa [G]tôi.<br />\r\nTôi chỉ [F]có một cuộc sống [C]này, với tất [F]cả tình [G]yêu mê [C]say.<br />\r\nNguyện dâng [F]hết lên cho Giê- [C]xu, để được [F]bên Chúa [G]suốt đêm [C]ngày</p>\r\n', 'C', 5, 'Tôi không có gì trong thế gian này, mà chỉ có', 'Tôi chỉ có Giê-xu mà thôi, nhưng trong Ngài', NULL, NULL, '', '2020-09-01 10:10:38', 1, '2020-09-01 10:10:53', 1),
(22, 'Vào Trong Nơi Thiêng Liêng', 'vao-trong-noi-thieng-lieng', '<p>[1]<br />\r\nVào [Em]trong nơi thiêng liêng nơi chí [D]thánh Cha.<br />\r\nBước [C]vào bằng huyết [D]Chiên Con bôi sạch [Em]lòng.<br />\r\nVào [Em]trong chỉ thờ tôn Vua Giê-[D]xu thôi.<br />\r\nBước [C]vào để chúc [D]tán danh Giê-[Em]xu.<br />\r\n[Chorus]<br />\r\nThờ phượng [G]Chúa Giê-[D]xu, [Am]Vua Giê-[Em]xu.<br />\r\nThờ phượng[G] Chúa Giê-[D]xu, [Am]Vua Giê-[Em]xu.<br />\r\n[Bridge]<br />\r\nThánh thay là Danh [C]của Ngài. [D]Thánh thay Giê-[Em]xu</p>\r\n', 'Em', 4, 'Vào trong nơi thiêng liêng, nơi chí Thánh Cha', 'Thờ phượng Chúa, Giê-xu. Vua Giê-xu', NULL, 80, '', '2020-09-01 10:12:40', 1, '2020-09-01 10:12:40', 1),
(23, 'Vua Hiển Vinh', 'vua-hien-vinh', '<p>[1]<br />\r\n[G]Chúa [D]Vua chí [Em]cao, Ngài [C]ngự [D]trên ngôi đời [G]đời.<br />\r\nQuyền [Bm]năng vinh hiển vô [Em]song, vạn [C]vật [Am]quỳ gối chúc [D]tôn.<br />\r\n[G]Chúa [D]Vua hiển [Em]vinh, trần [C]gian [D]suy tôn Danh [G]Ngài<br />\r\nLòng [Bm]con ca chúc Danh [Em]Ngài, thật [C]thánh thay [D]Vua Giê-[G]xu.<br />\r\n[Pre-Chorus]<br />\r\n[C]Lòng này ngày [D]đêm thức canh, [Bm]đợi chờ ngày [Em]Vua tái lâm.<br />\r\n[C]Lòng mong [D]ước Chúa mau trở [G]lại.<br />\r\n[C]Vì Ngài là [D]Vua các vua, [Bm]đại quyền đại [Em]năng hiển vinh.<br />\r\n[C]Ngài phục [Am]lâm nay [D]mai.<br />\r\n[Chorus]<br />\r\n[G]Ha-[D]lê-lu-[Em]gia! Ngợi [C]khen [D]Vua trên muôn [G]loài.<br />\r\nNgài [Bm]sẽ mau tái [Em]lâm, làm [C]Chúa quản [D]cai đời[G] đời.</p>\r\n', 'G', 4, 'Chúa Vua chí cao, Ngài ngự trên ngôi đời đời', 'Ha-lê-lu-gia, ngợi khen Vua trên muôn loài.', NULL, NULL, '', '2020-09-01 10:14:22', 1, '2020-09-01 10:14:22', 1),
(24, 'Diệu vinh thay là Vua Giê-xu', 'dieu-vinh-thay-la-vua-gie-xu', '<p>[1]<br />\r\n[G]Diệu vinh thay là Vua Giê-[D]xu, <br />\r\nNgài thật [Em]đáng tôn vinh trên [C]các từng trời rất[D] cao. (x2)<br />\r\n[Chorus]<br />\r\nGiê-[G]xu là [D]Vua hiển [Em]vinh. <br />\r\nKhắp [C]nơi nơi dâng lên muôn lời [D]ca.<br />\r\nLòng [G]con giờ [D]đây hướng về [Em]Ngài, <br />\r\nthờ phượng [C]Cha yêu với [D]cả tâm linh của [G]con</p>\r\n', 'G', 4, 'Diệu vinh thay là Vua Giê-xu, Ngài thật đáng tôn', 'Giê-xu là Vua hiển vinh, khắp đát trời dâng lên', NULL, NULL, '', '2020-09-01 10:16:41', 1, '2020-09-01 10:16:41', 1),
(25, 'Trước Oai Nghi Chúa', 'truoc-oai-nghi-chua', '<p>[1]<br />\r\n[Em]Đến [D]nơi [C]đây, [G/B]con khiêm cung quỳ trước mặt [C]Chúa. [G/B]Ơn Cha bao dung đã đem con [D/F#]về. [  B]<br />\r\n[Em]Đến [D]trước [C]Cha, [G/B]xưa con đây là thân tội [C]lỗi, [G/B]chính Chúa đổ huyết để đem con [D/F#]về. [  B]<br />\r\n[2] <br />\r\n[Em]Đến [D]trước [C]ngôi, [G/B]con khiêm cung nhỏ bé trước tình [C]Chúa. [G/B]Ơn yêu thương bao la Chúa đã thứ [D/F#]tha [  B]<br />\r\n[Em]Đến [D]với [C]Cha, [G/B]nay ước muốn được thuộc Ngài [C]mãi, [G/B]sống với thánh khiết hiển vinh rạng [D/F#]ngời. [  B]<br />\r\n[Prechorus]<br />\r\n[Em]Giờ [D/F#]con  [  G]tìm được Đấng yêu thương [Em]đời đời [D/F#]chẳng [G]phai, <br />\r\nKhi Chúa hy sinh vì [A]con, thân báu trên thập hình [C]xưa.<br />\r\n[Chorus]<br />\r\n[G]Đấng quyền [D]oai, [Em]Chúa muôn [C]loài, tình [G]ái Chúa đã cứu vớt tâm linh [D]con, <br />\r\nDầu thân con [Em]đây không xứng đáng chi cho [C]Ngài. <br />\r\n[G]Đấng quyền [D]oai, [Em]Chúa muôn [C]loài, tình [G]ái Chúa sẽ biến đổi tâm linh [D]con, <br />\r\nVà con [Em]sẽ mãi mãi ở bên Cha [C]hoài. Chúa yêu [G]ơi!</p>\r\n', 'Em', 4, 'Đến nơi đây, con khiêm cung quỳ trước mặt Chúa', 'Đấng Quyền oai, Chúa muôn loài. Tình ái Chúa đã', NULL, NULL, 'https://youtu.be/JpIi07kn_10', '2020-09-01 10:19:26', 1, '2020-09-01 10:26:14', 1),
(26, 'Thánh Linh Quyền Năng', 'thanh-linh-quyen-nang', '<p>[1]<br />\r\n[G]Thánh Linh Chúa Trời Quyền [Am7]năng vô song<br />\r\n[G]Thánh Linh vĩ đại Điểm [Am7]tô bầu [D]trời.<br />\r\n[G]Chính Cha hứa rằng Thần [Am7]Ngài ban cho<br />\r\nĐầy dẫy [C]khắp lòng người nào ngập tràn khát [D]khao.<br />\r\n[2]<br />\r\n[G]Thánh Linh năng quyền Dòng [Am7]sông nước sống<br />\r\n[G]Tuôn tràn muôn người Bằng [Am7]tình yêu bao la[D]<br />\r\n[G]Tâm này trông chờ Quyền [Am7]năng trên cao<br />\r\nVà Ngài [C]đến với tấm lòng tràn đầy khát [D]khao.<br />\r\n[Chorus]<br />\r\nThần Linh Chúa [G]dấu [Am7]yêu <br />\r\n[B7]Con đây luôn mãi khát khao [Em]Ngài <br />\r\nXin Ngài dẫy [C]đầy tâm linh [Bm]này <br />\r\nHỡi [D]Thánh Linh Quyền [G]Năng!!! </p>\r\n', 'G', 3, 'Thánh Linh Chúa Trời quyền năng vô song', 'Thần Linh Chúa dấu yêu, con đây luôn mãi ', NULL, NULL, 'https://www.youtube.com/watch?v=TFGoyzfYqJs', '2020-09-01 10:28:01', 1, '2020-09-01 10:28:01', 1),
(27, 'Trọn Đời Tôi Dâng Lên', 'tron-doi-toi-dang-len', '<p>[1]<br />\r\n[Em]Trọn đời tôi dâng lên Thiên Chúa, <br />\r\nCảm tạ [Am7]Chúa nhân từ, <br />\r\ntôi hát [B7]lên giống như Đa-Vít [Em]xưa. (x2)<br />\r\n[Chorus]<br />\r\n[E]Tôi sẽ [Am]hát, [D]tôi ngợi [G]ca! <br />\r\nHát ca [B7]ngợi Chúa tôi yêu [Em]kính.<br />\r\n[E]Tôi sẽ [Am]hát, [D]tôi ngợi [G]ca! <br />\r\nHát ca [B7]ngợi Chúa tôi nhân[Em] từ!</p>\r\n', 'Em', 3, 'Trọn đời tôi dâng lên Thiên Chúa cảm tạ Chúa nhân từ', 'Tôi sẽ hát, tôi ngợi ca', NULL, NULL, '', '2020-09-01 10:29:49', 1, '2020-09-01 10:29:49', 1),
(28, 'Hãy vỗ tay vui reo mừng lên', 'hay-vo-tay-vui-reo-mung-len', '<p>[1]<br />\r\n[Em]Hãy vỗ tay vui reo mừng [G]lên! [D]Vua chúng ta chính [Em]Giê-xu. [C]  [  D]  [    Em]<br />\r\n[Em]Hãy vỗ tay vui reo mừng [G]lên! [D]Vua chúng ta chính [Em]Giê-xu.<br />\r\nVì Ngài [C]trên ngôi Vua [D]đang trị [Em]vì.<br />\r\nVì Ngài [C]trên ngôi Vua [D]đang trị [Em]vì.<br />\r\n[Chorus]<br />\r\n[Em]Ngài đã phá [D]tan vua trần [C]gian, quyền ma [B7]vương.<br />\r\nNgài đắc [C]thắng quyền tối [D]tăm trong khải hoàn [Em]ca.<br />\r\n[Em]Mọi quyền năng chí [D]cao, chúc tôn, hiển [C]vinh thuộc duy [B7]Chúa! <br />\r\n[C]Ha-le-[D]lu-[Em]ja!<br />\r\n </p>\r\n', 'Em', 3, 'Hãy vỗ tay vui reo mừng lên', 'Ngài đã phá tan, vua trần gian, quyền ma vương', NULL, NULL, '', '2020-09-01 10:32:03', 1, '2020-09-01 10:32:03', 1),
(29, 'Ca vang danh Chúa', 'ca-vang-danh-chua', '<p>[1]<br />\r\n[Dm]Cùng nhau tung hô danh Chúa Giê-hô-va [A]Tsidkenu, <br />\r\nvì Ngài là [Am]Thượng Đế Đấng công bình muôn [Dm]đời. <br />\r\n[Dm]Cùng nhau tung hô danh Chúa Giê-hô-va [A]Nissi, <br />\r\nvì Ngài là cờ [Am]xí của những ai tin danh [Dm]Ngài. <br />\r\nGiê-hô- [Gm]va Salôm bình [Dm]an cho muôn người. <br />\r\nGiê-hô- [Gm]va Dirê nguồn cung [A7]cấp cho ai khó khăn.<br />\r\n[Chorus]<br />\r\nĐồng chúc [Dm]tán. Đồng ngợi [Gm]khen. <br />\r\nGiê-hô- [C]va Sama Chúa luôn hiện [F]diện.<br />\r\nGiê-hô- [Bb]va Rhapha Đấng chữa [Gm]lành bệnh tật và [Bb]bao tâm hồn cay [A7]đắng. <br />\r\nGiê-hô- [Dm]va Rôhi dẫn dắt dân Ngài, <br />\r\nlòng được [F]no nê, an vui bên thảm cỏ xanh, <br />\r\nhồn được tưới [Dm]mát thỏa mãn khát khao trong [Gm6]đời. <br />\r\nCùng nhau tung [A7]hô danh Giê-hô-va Chí [Dm]Cao.</p>\r\n', 'Dm', 3, 'Cùng nhau tung hô danh Chúa GIê-hô-va Tsidkenu', 'Đồng chúc tán, đồng ngợi khen. Giê-hô-va Sha-ma', NULL, NULL, '', '2020-09-01 14:36:09', 1, '2020-09-01 14:36:09', 1),
(30, 'Yeah... Con tự do', 'yeah-con-tu-do', '<p>[1]<br />\r\n[E]Tại thập tự giá Giê-xu chết [C#m]thay tội con. <br />\r\nChịu [A]biết bao nhục hình để gánh [B]thay cho con.<br />\r\n[E]Bệnh tật rủa sả với bao đớn [C#m]đau cuộc đời.<br />\r\nĐể [A]con được tự [B]do.<br />\r\n[chorus]<br />\r\nNgài luôn [E]muốn chữa lành, Ngài luôn [C#m]muốn chữa lành<br />\r\nbệnh tật đau yếu cho con.<br />\r\nTự do [E]khỏi gánh nặng, tự do [C#m]khỏi tuyệt vọng, <br />\r\ntự do [A]khỏi bệnh tật bất [B]an..<br />\r\n[E]Yeah!...Con tự do...<br />\r\n[bridge]<br />\r\n[E]Chẳng còn bệnh tật, [C#m]chẳng còn tuyệt vọng, <br />\r\n[A]chỉ còn nụ cười thỏa [B]vui. [E]Chẳng còn nghèo nàn, <br />\r\n[C#m]Chẳng còn buồn phiền, [A]chỉ còn nụ [B]cười vui [E]mừng.</p>\r\n', 'E', 3, 'Tại thập tự giá Giê-xu chết thay tội con', 'Ngài luôn muốn chữa lành, Ngài luôn muốn chữa lành', NULL, NULL, '', '2020-09-03 12:31:51', 1, '2020-09-03 12:31:51', 1),
(31, 'Đức tin trọn vẹn', 'duc-tin-tron-ven', '<p>[1]<br />\r\n[C]Con sẽ tìm [F]kiếm điều gì tuyệt vời [G]hơn tình yêu Cứu [C]Chúa?<br />\r\nLà [F]Đấng hiển [G]vinh mà chịu [E]phó thân [Am]vàng. Có [F]tình yêu nào dám [G]so. <br />\r\n[C]Giê-xu là [F]Vua muôn vua, chịu nhục [G]khinh và treo trên thập [Am]giá, <br />\r\nNhục [F]nhã đắng [G]cay gánh [E]lấy tội [Am]con, [F]Giê-xu danh yêu [G]thương diệu [C]kỳ. <br />\r\n[Chorus]<br />\r\nCòn lo [Am]lắng chi về ngày [Em]mai, vì tương [F]lai thuộc [G]Đấng yêu [C]thương, <br />\r\nXin giao [Am]phó Cha mọi nẻo [Em]đường. Lời Cha [F]phán là lời thành [G]tín. <br />\r\nCon tin nơi [Am]Ngài, Chúa [G]ơi, Chúa chẳng quên [Em]con bao [Am]giờ. <br />\r\nĐức [F]tin đơn sơ nơi [G]Ngài chẳng cần lo [Dm]nghĩ nhưng mãi [G]tin trọn [C]vẹn.</p>\r\n', 'C', 4, 'Con sẽ tìm kiếm điều gì tuyệt vời hơn tình yêu Cứu Chúa', 'Con tin nơi Ngài Chúa ơi, Chúa chẳng quên con bao giờ', NULL, NULL, '', '2020-09-03 13:40:00', 1, '2020-09-03 13:40:00', 1),
(32, 'Ngài là muôn nhu cầu tôi', 'ngai-la-muon-nhu-cau-toi', '<p>[1]<br />\r\n[D]Ngài là sức [A]thiêng cho tôi dựa [Bm]nương, Ngài là báu [F#m]vật mà tôi mong [G]tìm. <br />\r\nNgài là muôn [D]nhu [A]cầu [D]tôi. [A]<br />\r\n[D]Lòng tôi mến [A]yêu Chúa hơn ngọc [Bm]châu, Ngài tha hết [F#m]mọi tội lỗi trong [G]đời. <br />\r\nNgài là muôn [D]nhu [A]cầu [D]tôi.<br />\r\n[Chorus]<br />\r\n[D]Giê- [A]xu [Bm]là Chúa [F#m]tôi, [ G]Chúa có [D]danh [A]đẹp [D]thay. [A]<br />\r\n[D]Giê- [A]xu [Bm]là Chúa [F#m]tôi, [ G]quý báu [D]thay [A]danh [D]Ngài.<br />\r\n[2]<br />\r\n[D]Ngài mang hết [A]bao nhiêu ô nhục [Bm]tôi, Ngài đã sống [F#m]lại và sống muôn [G]đời, <br />\r\nNgài là muôn [D]nhu [A]cầu [D]tôi. [A]<br />\r\n[D]Hồi tôi vấp [A]chân Chúa nâng vực [Bm]tôi, hồi tim héo [F#m]tàn Ngài khiến tươi [G]cười. <br />\r\nNgài là muôn [D]nhu [A]cầu [D]tôi.</p>\r\n', 'D', 4, 'Ngài là sức thiêng cho con dựa nương, Ngài là báu vật', 'GIê-xu là Chúa con, Chúa có danh đẹp thay', NULL, 100, '', '2020-09-03 13:43:19', 1, '2020-09-03 13:43:58', 1),
(33, 'Giê-xu Vầng Đá chở che', 'gie-xu-vang-da-cho-che', '<p>[1]<br />\r\n[G]Giê-xu thiếu vắng [C]Ngài lòng [D]con bất [G]an. [C] [D]<br />\r\n[G]Giê-xu chính Chúa [C]là nguồn ơn cứu [D]giúp.<br />\r\nTừ khi [Em]có Cứu Chúa trong [G]đời. Dương [A]thế không ai sánh [C]bằng.<br />\r\nVượt trên [Am]hết mọi điều diệu [D]kỳ chỉ duy mình [G]Chúa Giê-[D]xu.<br />\r\n[Chorus]<br />\r\nGiê-[G]xu vầng [C]đá [G]chở [D]che. Giê-[Em]xu là [C]tất cả [G]cho cuộc [D]đời.<br />\r\nGiê-[G]xu vầng [C]đá [G]chở [D]che. [C]Điều chi quá khó [D]có Giê-xu làm [G]rồi.</p>\r\n', 'G', 3, 'Giê-xu thiếu vắng Ngài lòng con bất an, Giê-xu chính Chúa là', 'Giê-xu vầng đá chở che, Giê-xu là tất cả cho cuộc đời', NULL, 120, '', '2020-09-03 13:47:37', 1, '2020-09-03 13:47:37', 1),
(34, 'Chúa chính Ngài là Đấng con mãi tôn thờ', 'chua-chinh-ngai-la-dang-con-mai-ton-tho', '<p>[1]<br />\r\n[G]Chúa chính [A]Ngài là [F#m]Đấng con mãi tôn [Bm]thờ. [G]Chúa chính [A]Ngài là [D]Vua.<br />\r\n[G]Chúa chính [A]Ngài là [F#m]Đấng con mãi tôn [Bm]thờ. [D]Chúa chính [A]Ngài Vua lòng [D]con.<br />\r\n[Chorus]<br />\r\nLòng nguyện [G]hát chúc tán Chúa [D]muôn đời, lời ngợi [F#m]khen với cả tấm [Bm]lòng.<br />\r\nNguyện [G]dâng lên Cha muôn [A]đời con hằng chúc [D]tụng[D7].<br />\r\nLòng nguyện [G]hát chúc tán Chúa [D]muôn đời, lời ngợi [F#m]khen với cả tấm [Bm]lòng.<br />\r\nNguyện [G]dâng lên Cha muôn [A]đời con hằng chúc [D]tôn.</p>\r\n', 'G', 4, 'Chúa chính Ngài là Đấng con mãi tôn thờ, Chúa chính Ngài', 'Lòng nguyện hát chúc tán Chúa Vua muôn đời', NULL, 90, '', '2020-09-03 13:50:04', 1, '2020-09-03 13:50:04', 1),
(35, 'Thi thiên 100', 'thi-thien-100', '<p>[1]<br />\r\n[Am]Hỡi cả trái đất, hãy hát ngợi [Dm]khen<br />\r\nKhắp khắp các [F]nước cất tiếng ngợi [Am]khen<br />\r\nReo [Am]mừng cho Đức Giê-hô-[Dm]va.<br />\r\nReo [F]mừng cho Đức Giê-hô-[Am]va.<br />\r\n[chorus]<br />\r\nHỡi những [G]ai dân sự [Am]Ngài<br />\r\nLà bầy [Dm]chiên của đồng cỏ [Am]Ngài<br />\r\nChính chúng [G]tôi là dân sự [Am]Ngài<br />\r\nLà bầy [E]chiên của đồng cỏ [Am]Ngài<br />\r\nKhá hầu [Dm]việc Ngài cách vui [Am]mừng<br />\r\nHãy hát [G]xướng mà đến trước mặt [Am]Ngài.</p>\r\n', 'Am', 3, 'Hỡi cả trái đất hãy hát ngợi khen', 'Hỡi những ai là dân sự Ngài, là bầy chiên của đồng cỏ Ngài', NULL, 110, '', '2020-09-05 04:55:44', 1, '2020-09-05 04:55:44', 1),
(36, 'Quyền năng Chúa không đổi dời', 'quyen-nang-chua-khong-doi-doi', '<p>[1]<br />\r\n[Am]Quyền năng Chúa vẫn [F]không đổi dời. Thần Linh [Am]Chúa oai [F]nghi rạng ngời. <br />\r\nDanh Ngài [Am]vinh quang luôn toả [F]sáng khắp [G]trên trần [Am]gian.<br />\r\nBài ca [Am]đắc thắng ta [F]ca rộn ràng. Tụng ca [Am]Chúa chúc tôn [F]Cha đời đời. <br />\r\nTrong danh [Am]Giê-xu, chiến [F]thắng đã [G]thuộc về [Am]ta.<br />\r\n[chorus]<br />\r\n[Am]Đấng đắc thắng bóng tối chính [G]Giê-[Am]xu . Cùng nhau ta đi theo dấu [G]chân [Am]Ngài.<br />\r\nKhông chi có thể đứng [F]vững trước [G]danh [Am]Giê-xu.</p>\r\n', 'Am', 3, 'Quyền năng Chúa vẫn không đổi dời', 'Đấng đắc thắng bóng tối chính Giê-xu cùng nhau', NULL, 110, '', '2020-09-05 07:12:56', 1, '2020-09-05 07:13:29', 1),
(37, 'Chúc tôn Vua hiển vinh', 'chuc-ton-vua-hien-vinh', '<p>[1]<br />\r\n[Em]Cửa đời đời [C]nâng cao lên [D]Vua vinh quang đang hiện [B7]đến. <br />\r\nHỡi các [Em]cửa đời đời [C]nâng cao lên [D]Vua vinh hiển sẽ [Em]vào.<br />\r\n[Em]Thờ phượng Ngài  [G]tôn vinh Giê-xu [D]Vua muôn vua và [Em]Chúa các chúa. <br />\r\nChúc tôn [C]Ngài trọn lòng này [G]dâng vinh quang [B7]Ha-lê-lu-gia! Vua hiển [Em]vinh<br />\r\n[Chorus]<br />\r\nNào ta [G]hãy hát xướng ngợi [Am]danh JESUS, nào ta [D]hãy chúc tán thờ [Em]Vua muôn vua.<br />\r\nNgài đã [G]đến thế giới chuộc [Am]mua nhân gian Ngài xứng [D]đáng chúc tán bài [B7]ca khải [Em]hoàn. </p>\r\n', 'Em', 3, 'Cửa đời đời, nâng cao lên. Vua vinh quang', 'Nào ta hãy hát sướng ngợi danh Giê-xu', NULL, 120, '', '2020-09-05 07:15:35', 1, '2020-09-05 07:23:54', 1),
(38, 'Đứng lên dân Chúa', 'dung-len-dan-chua', '<p>[1]<br />\r\nĐứng [E]lên dân Thánh Chúa [C#m]Giê-xu. Đứng [F#m]lên hãy đứng sẵn [E]sàng.<br />\r\nNgày [F#m]mới đã về, ngày [C#m]mới huy hoàng. Ở [A]trong quyền năng đắc [B]thắng.<br />\r\nHãy [E]khá biết chính sức [C#m]mới mình. Hãy [F#m]khá biết năng lực [E]mình,<br />\r\nVì [F#m]Chúa trang bị quyền [C#m]phép dân Ngài. Đập [A]tan quyền lực [B]satan.<br />\r\n[chorus]<br />\r\nNgài ở [E]với [C#m]mình Chúa [F#m]thắng luôn luôn, tràn [A]đầy sức [B]mạnh<br />\r\nNgài ở [E]với [C#m]mình chính [F#m]Chúa tiên phong ở [A]trong đoàn quân đắc [B]thắng.<br />\r\nNgài ở [E]với [C#m]mình Chúa [F#m]thắng luôn luôn, tràn [A]đầy sức [B]mạnh<br />\r\nNgài ở [E]với [C#m]mình chính [F#m]Chúa tiên phong ở [A]trong quân [B]đoàn khải [E]hoàn.</p>\r\n', 'E', 3, 'Đứng lên dân Thánh Chúa Giê-xu, đứng lên hãy', 'Ngài ở với mình, Chúa Thánh luôn luôn tràn đầy', NULL, 120, '', '2020-09-05 07:32:34', 1, '2020-09-05 07:33:21', 1),
(39, 'Toàn năng thay Chúa', 'toan-nang-thay-chua', '<p>[1]<br />\r\nToàn năng [C]thay Chúa [G/B]Giê-xu [Am]năng quyền<br />\r\nLuôn nâng [Dm]đỡ dẫn dắt giúp [Fm]sức con ngày [C]đêm[G7]<br />\r\nToàn năng [C]thay Chúa [Em]vinh quang [Am]đáng ngợi<br />\r\nMọi điều [Bb]Cha luôn ban phước dẫn[Am] dắt [D7/F#]suốt ngày [Gsus4]đêm[   G]<br />\r\n[chorus]<br />\r\nGiê-xu [C]năng quyền [E7]trên hết [Am]năng quyền [F]<br />\r\nĐáp [C]lời mọi điều vượt [E7]quá suy nghĩ lòng [Am]con[  Fm]<br />\r\nGiê-xu [C]năng quyền, [G7]Chúa, Vua [Am]năng quyền<br />\r\nDìu [Dm]con đi trong ý [G7]Cha luôn ngày [C]đêm</p>\r\n', 'C', 4, 'Toàn năng thay Chúa, Giê-xu năng quyền', 'Giê-xu năng quyền, trên hết năng quyền. Đáp lời', NULL, 80, '', '2020-09-05 07:41:33', 1, '2020-09-05 13:35:10', 1),
(40, 'Chúa yêu Ngài hơn cả mọi điều', 'chua-yeu-ngai-hon-ca-moi-dieu', '<p>[1]<br />\r\n[D]Chúa yêu [A]Ngài hơn cả mọi [Bm]điều<br />\r\n[G]Vàng có nghĩa [D]gì [Em]thế giới kia là [A]chi<br />\r\n[D]Chúa con [F#m]nguyện yêu cha hết [Bm]lòng<br />\r\n[Em]Sẽ không bao giờ [D]để mất Ngài<br />\r\n[G]Sẽ không bao giờ [A]xa rời [D]Ngài[ D7]<br />\r\n[chorus]<br />\r\nTình yêu cha [G]quá lớn [A]lớn lao hơn cả [D]thiên đàng [D7]<br />\r\nLòng thương xót [G]cứu Chúa [Bm]thấm sâu hơn biển [A]hồ [ D7]<br />\r\nVà ân điển [G]quí giá [A]bao la hơn cả [F#m]thế giới [Bm]này<br />\r\n[G]Sẽ không bao giờ [D]để mất Ngài<br />\r\n[G]Sẽ không bao giờ [A]xa rời [D]Ngài<br />\r\n[1]<br />\r\n[A]Lord, you’re [A]more than any-[Bm]thing<br />\r\n[G]You’re more than [D]gold [Em]more than any-[A]thing<br />\r\n[D]Lord, you’re [F#m]everything to [Bm]me<br />\r\n[Em]I will never [D]let you go<br />\r\n[G]Never ever [A]let you [D]go[ D7]<br />\r\n[chorus]<br />\r\nYour lover is [G]higher, [Bm]higher than a hea-[A]ven[D7]<br />\r\nYour lover is [G]mercy’s [Bm]deeper deeper than the [A]earth[D7]<br />\r\nYour grace is [G]wider [A]wider than the [F#m]o-[   Bm]cean<br />\r\n[G]I will never [D]let you go<br />\r\n[G]Never ever [A]let you [D]go</p>\r\n', 'D', 4, 'Chúa yêu Ngài hơn cả mọi điều', 'Tình yêu Cha quá lớn, lớn lao hơn cả thiên đàng', NULL, 100, '', '2020-09-05 07:51:29', 1, '2020-09-05 07:52:46', 1),
(41, 'Giê-xu muôn đời', 'gie-xu-muon-doi', '<p>[1]<br />\r\nKhắp đất [C]trời hòa dâng tiếng [G]ca<br />\r\nChúc tôn [Am]Ngài là Vua hiển [Em]vinh <br />\r\nMọi đầu [F]gối [G]sẽ [Em]quỳ nơi chân [Am]Ngài<br />\r\nMọi môi [F]lưỡi tôn [G]quí<br />\r\n[Chorus]<br />\r\nNgài là [Am]Chúa các chúa, là [Em]Vua muôn vua<br />\r\nGiê-[F]xu vinh [G]hiển thay [C]Ngài!<br />\r\nThập tự [Am]giá Chúa gánh, chịu [Em]thay cho con<br />\r\nGiê-[F]xu yêu ơi, Ngài [G]vĩ đại [Am]thay!<br />\r\n[2]<br />\r\nCảm ơn [C]Ngài lòng con thái [G]an<br />\r\nChúa nhân [Am]từ hằng săn sóc [Em]con<br />\r\nTình yêu [F]Giê-[G]xu [Em]mãi không phai [Am]tàn<br />\r\nLòng mừng [F]vui con [G]hát<br />\r\n[Bridge]<br />\r\nLòng con [Am]yêu Ngài và suy [F]tôn Ngài<br />\r\nGiê-xu [C]muôn đời, Giê-xu [G]muôn đời </p>\r\n', 'C', 4, 'Khắp đất trời hòa dâng tiếng ca', 'Ngài là Chúa các chúa, là Vua muôn vua', NULL, 80, '', '2020-09-05 07:56:00', 1, '2020-09-05 07:59:41', 1),
(42, 'Trong nơi vinh hiển', 'trong-noi-vinh-hien', '<p>[1]<br />\r\n[G]Nơi đây con đang ngắm [D]xem, Cha của [C]con Đức Chúa [G]Trời[D]<br />\r\n[Em]Trong ơn yêu thương lớn [Bm]lao, Ngài phó [C]con yêu dấu [G]Ngài Giê-[D]xu.<br />\r\n[G]Tâm con dâng lên khúc [D]ca, nguyện tán [C]dương duy chính [G]Ngài.[D]<br />\r\n[Em]Nguyện xin dâng lên chính [Bm]con, cùng với [C]bao điều cao quý [D]nhất.<br />\r\n[chorus]<br />\r\nGiơ cao [G]đôi tay chúc [D]tôn với tất [Bm]cả lời tôn [Em]kính.<br />\r\nNgợi [C]khen Cha oai nghi cao quý [D]thay.<br />\r\nGiờ được chiêm [G]ngưỡng Chúa với [D]dung quang xinh [Bm]tươi đầy vinh [Em]hiển<br />\r\n[C]Sẽ mãi luôn tôn [D]thờ Cha [Em]Thánh. [C]El-Shaddai Chúa [D]Cha toàn [G]năng.</p>\r\n', 'G', 4, 'Nơi đây con đang ngắm xem, Cha của con Đức Chúa Trời', 'Giơ cao đôi tay chúc tôn với tất cả lời tôn kính', NULL, 90, 'https://youtu.be/aZfnoeE383Q', '2020-09-12 13:08:27', 1, '2020-09-12 13:08:27', 1),
(43, 'Christ là lời giải đáp', 'christ-la-loi-giai-dap', '<p>[1]<br />\r\n[C]Christ là lời đáp cho những điều tôi ngóng [G]trông. Christ là lời [C]đáp cho hết mọi nhu cầu. <br />\r\nGiê-xu cứu thế [C]nhân, Đấng danh y [F]lớn lao. Ồ, ha-lê-[G]lu-gia![G7] Chúa ban dồi [C]dào.  </p>\r\n', 'C', 4, 'Christ là lời đáp cho những điều con ngóng trông', 'Ô Ha-lê-lu-gia Chúa ban dồi dào', NULL, 90, '', '2020-09-12 13:14:57', 1, '2020-09-12 14:25:09', 1),
(44, 'Giê-hô-va bình an', 'gie-ho-va-binh-an', '<p>[1]<br />\r\n[Em]Giê-hô-va Bình An. [C]Bình An Giê-hô-[D]va Sa-[Em]lôm. <br />\r\nÔi! [Am]Chúa Bình An! Ta [Em]chiên thơ của Ngài, vì Giê-[C]xu là bình [D]an, chân [Em]lý. <br />\r\n[chorus]<br />\r\nTa [Am]không còn bối rối. Ta [Em]không còn lo lắng. Ta [C]không còn bất [D]an với lo [Em]buồn. <br />\r\nGiê-[Am]xu đã đắc thắng, Ngài [Em]đã ban an bình cho ai [C]nương cậy [D]nơi danh [Em]Ngài.</p>\r\n', 'Em', 3, 'Giê-hô-va bình an, bình an Giê-hô-va Shalom', 'Ta không còn bối rối, ta không còn lo lắng', NULL, 120, '', '2020-09-12 13:18:57', 1, '2020-09-12 13:24:15', 1),
(45, 'Vì Chúa toàn năng', 'vi-chua-toan-nang', '<p>[1]<br />\r\n[G]Ngài xứng đáng được tôn thờ và đáng [Em]chúc tụng<br />\r\nGiờ chúng [C]con đưa cao đôi [Am]tay mình và chúc [C]tán Giê-xu Chúa [D]thánh<br />\r\n[chorus]<br />\r\nVì Chúa toàn [G]năng Ngài làm những phép lạ lớn [Em]lao<br />\r\nNgoài ra Chúa không ai như [C]Ngài[C] [Bm] [Am]<br />\r\n[ C]Ngoài ra Chúa không ai như [D]Ngài.<br />\r\nVì Chúa toàn [G]năng, Ngài làm những phép lạ lớn [Em]lao.<br />\r\nNgoài ra Chúa không ai như [C]Ngài. [C] [Bm] [Am]<br />\r\n[C]Nào ai sánh Giê-[D]xu toàn [G]năng</p>\r\n', 'G', 4, 'Diệu vinh thay là Giê-xu Ngài thật đáng ngợi', 'Vì Chúa toàn năng, Ngài làm những phép lạ lớn lao', NULL, 90, '', '2020-09-12 13:21:53', 1, '2020-09-12 14:21:38', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `topic`
--

DROP TABLE IF EXISTS `topic`;
CREATE TABLE IF NOT EXISTS `topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_topic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic` (`topic_name`),
  UNIQUE KEY `slug_topic` (`slug_topic`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `topic`
--

INSERT INTO `topic` (`id`, `topic_name`, `slug_topic`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'Giáng Sinh', 'giang-sinh', '2020-08-25 16:58:48', 1, '2020-08-25 16:58:48', 1),
(2, 'Phục sinh', 'phuc-sinh', '2020-09-01 09:27:17', 1, '2020-09-01 09:27:17', 1),
(3, 'Thương khó', 'thuong-kho', '2020-09-01 09:27:28', 1, '2020-09-01 09:27:28', 1),
(5, 'Cha mẹ', 'cha-me', '2020-09-01 09:29:58', 1, '2020-09-01 09:29:58', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `type`
--

INSERT INTO `type` (`id`, `type_name`, `slug_type`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(3, 'Ngợi Khen', 'ngoi-khen', '2020-08-25 16:46:23', 1, '2020-08-25 16:46:23', 1),
(4, 'Thờ Phượng', 'tho-phuong', '2020-08-25 16:48:37', 1, '2020-08-25 16:48:37', 1),
(5, 'Đơn ca', 'don-ca', '2020-09-01 09:28:01', 1, '2020-09-01 09:28:01', 1),
(6, 'Dâng hiến', 'dang-hien', '2020-09-01 09:28:10', 1, '2020-09-01 09:28:10', 1),
(7, 'Tôn vinh', 'ton-vinh', '2020-09-01 09:28:22', 1, '2020-09-01 09:28:22', 1),
(9, 'Chiến thắng', 'chien-thang', '2020-09-01 09:28:44', 1, '2020-09-01 09:28:44', 1),
(10, 'Vui mừng', 'vui-mung', '2020-09-01 09:29:11', 1, '2020-09-01 09:29:11', 1),
(11, 'Bình an', 'binh-an', '2020-09-01 09:29:21', 1, '2020-09-01 09:29:21', 1),
(12, 'Vui vẻ', 'vui-ve', '2020-09-01 09:29:31', 1, '2020-09-01 09:29:31', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'hSh0vR30_VzwZoPFDqkj7lwnYY_dYSSR', '$2y$13$v3zOmDADZaIaqEXXvr2/HubTXCHNm5XuIA5wKSmuKCmTsQWYku9Ju', NULL, 'quan.1991.nguyen@gmail.com', 10, 1595698108, 1598948766, 'ti0atNJlwjZU-54h4j1kgeBv2vabBXLi_1595698108'),
(3, 'demo', 'uoFjbsBBai4YEVoz0k670vnQdh8TAFHH', '$2y$13$RVTkGmSHfTFDKRSiSo.n5ep/DBEPDQOrUPGidB8V2d1DHRYHRGB6e', NULL, 'demo@localhost.com', 10, 1598689790, 1598940864, 'UBcL15N4JbmL96_bRxi0YfJjM0iKQkbv_1598689790');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
