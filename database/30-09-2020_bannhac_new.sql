-- MySQL Workbench Synchronization
-- Generated: 2020-09-30 11:22
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: HP

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `sep13_webbannhac` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_type` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL COMMENT 'Title Type',
  `slug` VARCHAR(70) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_topic` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(70) NOT NULL COMMENT 'Title Topic',
  `slug` VARCHAR(70) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_keychords` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL COMMENT 'Primary Key Chord',
  `slug` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_keytags` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL COMMENT 'Key tags',
  `slug` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_song` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL COMMENT 'Title Song',
  `slug` VARCHAR(150) NULL DEFAULT NULL,
  `frist_lyric` VARCHAR(150) NOT NULL DEFAULT 'Chưa cập nhật' COMMENT 'Lời đầu của bài hát',
  `chorus_lyric` VARCHAR(150) NOT NULL DEFAULT 'Chưa cập nhật' COMMENT 'Điệp khúc bài hát',
  `lyric` TEXT NULL DEFAULT NULL COMMENT 'Lời bài hát',
  `created_date` DATETIME NOT NULL COMMENT 'Ngày tạo bài hát',
  `updated_date` DATETIME NULL DEFAULT NULL COMMENT 'Ngày sửa bài hát',
  `type_id` INT(11) NOT NULL,
  `topic_id` INT(11) NOT NULL,
  `keychords_id` INT(11) NOT NULL,
  `tempo_id` INT(11) NOT NULL,
  `linksong_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sep13_song_sep13_type_idx` (`type_id` ASC),
  INDEX `fk_sep13_song_sep13_topic1_idx` (`topic_id` ASC),
  INDEX `fk_sep13_song_sep13_keychords1_idx` (`keychords_id` ASC),
  INDEX `fk_sep13_song_sep13_tempo1_idx` (`tempo_id` ASC),
  INDEX `fk_sep13_song_sep13_linksong1_idx` (`linksong_id` ASC),
  INDEX `fk_sep13_song_sep13_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_sep13_song_sep13_type`
    FOREIGN KEY (`type_id`)
    REFERENCES `sep13_webbannhac`.`sep13_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_song_sep13_topic1`
    FOREIGN KEY (`topic_id`)
    REFERENCES `sep13_webbannhac`.`sep13_topic` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_song_sep13_keychords1`
    FOREIGN KEY (`keychords_id`)
    REFERENCES `sep13_webbannhac`.`sep13_keychords` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_song_sep13_tempo1`
    FOREIGN KEY (`tempo_id`)
    REFERENCES `sep13_webbannhac`.`sep13_tempo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_song_sep13_linksong1`
    FOREIGN KEY (`linksong_id`)
    REFERENCES `sep13_webbannhac`.`sep13_linksong` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_song_sep13_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `sep13_webbannhac`.`sep13_user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_tempo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `tempo` INT(11) NOT NULL DEFAULT 0 COMMENT 'Tempo',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_linksong` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `link` VARCHAR(255) NOT NULL DEFAULT 'Chưa cập nhật' COMMENT 'Link song',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_worship_day` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL COMMENT 'Date Worship\n',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_worshipsong_day` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `worship_day_id` INT(11) NOT NULL,
  `song_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sep13_worshipsong_day_sep13_worship_day1_idx` (`worship_day_id` ASC),
  INDEX `fk_sep13_worshipsong_day_sep13_song1_idx` (`song_id` ASC),
  CONSTRAINT `fk_sep13_worshipsong_day_sep13_worship_day1`
    FOREIGN KEY (`worship_day_id`)
    REFERENCES `sep13_webbannhac`.`sep13_worship_day` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_worshipsong_day_sep13_song1`
    FOREIGN KEY (`song_id`)
    REFERENCES `sep13_webbannhac`.`sep13_song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_keytags_song` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `song_id` INT(11) NOT NULL,
  `keytags_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_sep13_keytags_song_sep13_song1_idx` (`song_id` ASC),
  INDEX `fk_sep13_keytags_song_sep13_keytags1_idx` (`keytags_id` ASC),
  CONSTRAINT `fk_sep13_keytags_song_sep13_song1`
    FOREIGN KEY (`song_id`)
    REFERENCES `sep13_webbannhac`.`sep13_song` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_sep13_keytags_song_sep13_keytags1`
    FOREIGN KEY (`keytags_id`)
    REFERENCES `sep13_webbannhac`.`sep13_keytags` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `sep13_webbannhac`.`sep13_user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `auth_key` VARCHAR(45) NULL DEFAULT NULL,
  `password_hash` VARCHAR(255) NOT NULL,
  `password_reset_token` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NOT NULL,
  `status` INT(11) NOT NULL DEFAULT 9,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `role` ENUM('admin', 'manager', 'user') NOT NULL DEFAULT 'user',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
