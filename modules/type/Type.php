<?php

namespace app\modules\type;

/**
 * users module definition class
 */
class Type extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */

    public $controllerNamespace = 'app\modules\type\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
