<?php

namespace app\modules\song\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\song\models\Song;

/**
 * SongSearch represents the model behind the search form of `app\modules\song\models\Song`.
 */
class SongSearch extends Song
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'topic_id', 'tempo', 'created_by', 'updated_by'], 'integer'],
            [['title', 'slug', 'key_chord', 'first_lyric', 'chorus', 'link_song', 'created_date', 'updated_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Song::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'topic_id' => $this->topic_id,
            'tempo' => $this->tempo,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'updated_date' => $this->updated_date,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'key_chord', $this->key_chord])
            ->andFilterWhere(['like', 'first_lyric', $this->first_lyric])
            ->andFilterWhere(['like', 'chorus', $this->chorus])
            ->andFilterWhere(['like', 'link_song', $this->link_song]);

        return $dataProvider;
    }
}
