<?php

namespace app\modules\song\models;

use Yii;

/**
 * This is the model class for table "song".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $key_chord
 * @property int $type_id
 * @property string $first_lyric
 * @property string $chorus
 * @property int $topic_id
 * @property int $tempo
 * @property string $link_song
 * @property string $create_date
 * @property int $create_by
 * @property string $update_date
 * @property int $update_by
 */
class Song extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'song';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'key_chord', 'first_lyric', 'chorus','lyric'], 'required'],
            [['type_id', 'topic_id', 'tempo', 'created_by', 'updated_by'], 'integer'],
            [['created_date', 'updated_date'], 'safe'],
            [['title', 'slug', 'key_chord', 'first_lyric', 'chorus', 'link_song'], 'string', 'max' => 255],
            ['lyric','string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'lyric' => Yii::t('app', 'Lyric'),
            'key_chord' => Yii::t('app', 'Key Chord'),
            'type_id' => Yii::t('app', 'Type ID'),
            'first_lyric' => Yii::t('app', 'First Lyric'),
            'chorus' => Yii::t('app', 'Chorus'),
            'topic_id' => Yii::t('app', 'Topic ID'),
            'tempo' => Yii::t('app', 'Tempo'),
            'link_song' => Yii::t('app', 'Link Song'),
            'created_date' => Yii::t('app', 'Create Date'),
            'created_by' => Yii::t('app', 'Create By'),
            'updated_date' => Yii::t('app', 'Update Date'),
            'updated_by' => Yii::t('app', 'Update By'),
        ];
    }
    public function to_slug($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }
}
