<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\modules\user\models\User;

$getusernumber = new User;

$getuser = $getusernumber->getusernumber();


/* @var $this yii\web\View */
/* @var $model app\modules\song\models\Song */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7"style="z-index: 999;">
              <h6 class="h2 text-white d-inline-block mb-0"><?= Html::encode($this->title) ?></h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="<?php echo Yii::$app->urlManager->createUrl('site/index'); ?>"><?php echo Yii::t('app','List of songs') ?></a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?= Html::encode($this->title) ?></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
<!-- Header -->
    <div class="container-fluid mt--9">
<div id="invoice">
    <div class="toolbar hidden-print row">
        <?php if(!Yii::$app->user->getIsGuest()){ ?>
        <div class="col-lg-12 col-md-5 col-12 text-right">
          <?php if(Yii::$app->user->getId()!=$getuser){ ?>
            <div class="mb-2 mt-4 my-md-auto d-inline pr-md-1">
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
            'confirm' => Yii::t('app', 'Are you sure you want to delete this song?'),
            'method' => 'post',
            ],
            ]) ?>
            </div>
          <?php } ?>
            <div class="d-none d-lg-inline pl-md-1">
            <button id="printInvoice" class="btn btn-secondary" onclick="printJS({
            printable: 'song',//chọn id cần in
            type: 'html',
            header: '<?= Html::encode($this->title) ?>',
            css: ['<?php echo Yii::getAlias('@web').'/dist/css/style.css' ?>','<?php echo Yii::getAlias('@web').'/dist/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css' ?>','<?php echo Yii::getAlias('@web').'/dist/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css' ?>','<?php echo Yii::getAlias('@web').'/dist/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css' ?>','<?php echo Yii::getAlias('@web').'/dist/css/argon.css' ?>'], //Thêm css
            ignoreElements:['show_hide','transposition','title-song'], //loại trừ các element id này
            style: 'div#lyric p,div#lyric span{font-size:30px}span.verse,span.note-lyric{display:none}.name a{font-size:30px;text-decoration:none;font-weight:300;color:blue}'// chỉnh sửa lại một số css không hiển thị tốt
            })"><i class="fa fa-print"></i> <?php echo Yii::t('app','Print') ?></button>
            <button class="btn btn-secondary" onclick="exportPdf(<?php $model->title ?>)"><i class="fa fa-file-pdf-o"></i> <?php echo Yii::t('app','Export as PDF') ?></button>
            </div>
        </div>
          <?php } ?>
        <hr>
    </div>
    <div class="invoice overflow-auto" id="song">
        <div class="min-md-width">
            <header>
                <div class="row">
                    <div class="col">
                        <div class="title" id="title-song">
                            <h1><?= $this->title ?></h1>
                        </div>
                        <h2 class="name">
                            <a target="_blank" href="#">
                                <?php echo Yii::t('app','Key Primary') ?>: <?= $model->key_chord ?>
                            </a>
                        </h2>
                        <button class="btn btn-info" id="show_hide"><?php echo Yii::t('app','Hide chords') ?></button>
                    </div>
                </div>
            </header>
            <main>
                <div class="row">
                    <div class="col-12 song" id="lyric">
                        <div id="transposition" class="transposition"></div>
                        <?= Html::decode($model->lyric) ?>
                    </div>
                </div>
            </main>
            <footer>
                <a class="btn btn-outline-dark btn-sm float-left" href="<?php echo Yii::$app->urlManager->createUrl('site/index'); ?>"><i class="ni ni-bold-left"></i> <?php echo Yii::t('app','Back') ?></a>
                <?php  $user = user::find()->select('username')->where(['id' => $model->created_by])->one(); ?>
                <?php echo Yii::t('app','Posted by') ?>: <?php echo $user['username']; ?>
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
    </div>
</div>
</div>
<!-- <div class="song-view">

    <h1></h1>

    <p>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'lyric',
            'key_chord',
            'type_id',
            'first_lyric',
            'chorus',
            'topic_id',
            'tempo',
            'link_song',
        ],
    ]) ?>

</div>
 -->