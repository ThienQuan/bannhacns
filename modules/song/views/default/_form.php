<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\ArrayHelper;
use app\modules\type\models\Type;
use app\modules\topic\models\Topic;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\bootstrap4\Modal;
 

/* @var $this yii\web\View */
/* @var $model app\modules\song\models\Song */
/* @var $form yii\widgets\ActiveForm */
$get_arr_type = ArrayHelper::map(type::find()->all(),'id','type_name');
$get_arr_topic = ArrayHelper::map(topic::find()->all(),'id','topic_name');
if (Yii::$app->requestedRoute != 'song/default/create'){
    $save = "Cập nhật";
    $class = "btn btn-primary my-3 btn-sm";
}else{
    $save = "Thêm bài hát";
    $class = "btn btn-success my-3 btn-sm";
}
?>

<!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0">Thêm bài hát mới</h3>
        </div>
        <!-- Card body -->
        <div class="card-body">
    <?php $form = ActiveForm::begin(); ?>
          <!-- Form groups used in grid -->
          <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'title',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="song-title">Tên bài hát</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "Tiêu đề bài hát"]) ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'key_chord',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="key-chord">Hợp âm chủ</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "G,A,C..."]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type_id',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="song-type_id">Thể loại</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->dropDownList(
                    $get_arr_type,
                     ['prompt'=>'Chọn thể loại']); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
            <?= $form->field($model, 'first_lyric',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="first_lyric">Lời đầu bài hát</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "Lời 1 của bài hát."]) ?>
            </div>
            <div class="col-md-4">
            <?= $form->field($model, 'chorus',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="chorus">Điệp khúc</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "Câu đầu tiên của điệp khúc."]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'topic_id',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="song-topic_id">Chủ đề</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->dropDownList(
                    $get_arr_topic,
                     ['prompt'=>'Chọn chủ đề']); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-6">
                <?= $form->field($model, 'tempo',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="tempo">Tốc độ bài hát (bmp)</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "120"]) ?>
            </div>
            <div class="col-6">
                <?= $form->field($model, 'link_song',[ 'template' => '
                    <div class="form-group">
                        <label class="form-control-label" for="link_song">Link bài hát</label>
                        {input}
                    </div>
                    <div class="text-danger">{error}</div>'
                             ])->textInput(['maxlength' => true,'placeholder' => "VD: https://www.youtube.com/watch?v=XNswhKgId1Y"]) ?>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="form-group">
                 <!-- Text editor -->
                    <label class="form-control-label" for="song-lyric">Nội dung bài hát</label>
                     <?= $form->field($model, 'lyric')->widget(CKEditor::className(), [
                        'options' => ['rows' => 6],
                        'preset' => 'basic',
                    ])->label(false) ?>
                    <?= Html::submitButton(Yii::t('app', $save), ['class' => $class]) ?>
                    <?php if (Yii::$app->requestedRoute == 'song/default/create'){ ?>
                    <?= Html::resetButton(Yii::t('app','Reset'), ['class' => 'btn btn-primary my-3 btn-sm']) ?>
                    <?php } ?>
                    <a href="<?php echo Yii::$app->urlManager->createUrl('site/index'); ?>" class="btn btn-close btn-sm"><span class="glyphicon glyphicon-chevron-left"></span> Thoát</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php ActiveForm::end(); ?>