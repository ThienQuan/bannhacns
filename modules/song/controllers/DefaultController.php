<?php

namespace app\modules\song\controllers;

use Yii;
use app\modules\song\models\Song;
use app\modules\song\models\SongSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

/**
 * DefaultController implements the CRUD actions for Song model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Song models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $searchModel = new SongSearch();
        // $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        // return $this->render('index', [
            // 'searchModel' => $searchModel,
            // 'dataProvider' => $dataProvider,
        // ]);
        return $this->redirect(['site/index']);

    }

    /**
     * Displays a single Song model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Song model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Song();

        if ($model->load(Yii::$app->request->post())) {
        	// echo "<pre>";print_r(Yii::$app->request->post());die;
        	$model->slug =  $model->to_slug(Yii::$app->request->post()['Song']['title']);

        	$model->lyric = html_entity_decode(Yii::$app->request->post()['Song']['lyric']);
        	$model->created_date = date('Y-m-d H:i:s');
        	$model->created_by = Yii::$app->user->getId();
        	$model->updated_date = date('Y-m-d H:i:s');
        	$model->updated_by = Yii::$app->user->getId();
        	if($model->save()){
	            return $this->redirect(['view', 'id' => $model->id]);
        	}
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Song model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
        	$model->slug =  $model->to_slug(Yii::$app->request->post()['Song']['title']);
        	$model->lyric = html_entity_decode(Yii::$app->request->post()['Song']['lyric']);
        	$model->updated_date = date('Y-m-d H:i:s');
        	$model->updated_by = Yii::$app->user->getId();
        	if($model->save()){
	            return $this->redirect(['view', 'id' => $model->id]);
	        }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Song model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Song model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Song the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Song::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionTest()
    {
        return $this->render('test');
    }
}
