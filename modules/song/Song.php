<?php

namespace app\modules\song;

/**
 * users module definition class
 */
class Song extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */

    public $controllerNamespace = 'app\modules\song\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
