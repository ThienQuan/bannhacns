<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\web\View;

app\themes\adminlte3\assets\AdminleAsset::register($this);
app\assets\AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (Yii::$app->requestedRoute == 'site/login' || Yii::$app->requestedRoute == 'site/forgotpassword'){ ?>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="<?php echo Yii::getAlias('@web').'/js/plugins/nucleo/css/nucleo.css' ?>" rel="stylesheet" />
        <link href="<?php echo Yii::getAlias('@web').'/js/plugins/@fortawesome/fontawesome-free/css/all.min.css' ?>" rel="stylesheet" />
        <!-- CSS Files -->
        <link href="<?php echo Yii::getAlias('@web').'/css/argon-dashboard.css?v=1.1.2' ?>" rel="stylesheet" />
    <?php } ?>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
    <?php if (Yii::$app->requestedRoute != 'site/login' && Yii::$app->requestedRoute != 'site/forgotpassword'){ ?>
<body class="hold-transition sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand bg-gradient-primary pt-3 pb-3">
        <?php if (isset($this->blocks['header'])): ?>
            <?= $this->blocks['header'] ?>
        <?php else: ?>
            ... default content for header ...
        <?php endif; ?>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-light-primary navbar-vertical">
        <?php if (isset($this->blocks['left_menu'])): ?>
            <?= $this->blocks['left_menu'] ?>
        <?php else: ?>
            ... default content for menu ...
        <?php endif; ?>
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="main-content">
            <div class="container-fluid">
                <?= $content ?>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->

</div>
<!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
<?php }else{ ?>
    <?php if (Yii::$app->requestedRoute == 'site/login'||Yii::$app->requestedRoute == 'site/forgotpassword'){ ?>
    <body class="container-fluid" style="padding-right: 15px; padding-left: 15px;">
    <?php $this->beginBody() ?>
            
    <?= $content ?>
      <!--   Core   -->
      <script src="<?php echo Yii::getAlias('@web').'/js/plugins/jquery/dist/jquery.min.js' ?>"></script>
      <script src="<?php echo Yii::getAlias('@web').'/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
      <!--   Optional JS   -->
      <!--   Argon JS   -->
      <script src="<?php echo Yii::getAlias('@web').'/js/argon-dashboard.min.js?v=1.1.2' ?>"></script>
      <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
      <script>
        window.TrackJS &&
          TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
          });
      </script>
    <?php $this->endBody() ?>
    </body>
<?php }
} ?>
</html>
<?php $this->endPage() ?>
