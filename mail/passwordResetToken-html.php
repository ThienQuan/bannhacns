<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$getusername = $user->username;
$img = $message->embed($logo);
$this->title = "Reset Password";
?>
      <div class="align-center">
         <img src="<?= $img ?>" width="250px">
      </div>
<div class="content">

  <!-- START CENTERED WHITE CONTAINER -->
  <table role="presentation" class="main">

    <!-- START MAIN CONTENT AREA -->
    <tr>
      <td class="wrapper">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <p><?php echo Yii::t('app',"Hello").' '.$user->username ?></p>
              <p><?php echo Yii::t('app',"Someone requested that the password for your account be reset.") ?></p>
              <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
                <tbody>
                  <tr>
                    <td align="left">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td> 
                             <a href="<?php echo $verifyLink; ?>" target="_blank"><?php echo Yii::t('app',"Password reset") ?></a> 
                           </td>
                         </tr>
                       </tbody>
                     </table>
                   </td>
                 </tr>
               </tbody>
             </table>
             <p><?php echo Yii::t('app',"If you didn't request this, you can ignore this email or let us know. Your password won't change until you create a new password") ?></p>
             <p><?php echo Yii::t('app',"Best regards") ?>.</p>
           </td>
         </tr>
       </table>
     </td>
   </tr>

   <!-- END MAIN CONTENT AREA -->
 </table>
 <!-- END CENTERED WHITE CONTAINER -->

 <!-- START FOOTER -->
 <div class="footer">
  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td class="content-block">
        <span class="apple-link">Hội Thánh Nguồn Sống, 636 Ngọc Lâm, Long Biên, Hà Nội</span>
        <!-- <br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. -->
      </td>
    </tr>
    <tr>
      <td class="content-block powered-by">
        <?= Html::a(Yii::t('app',"Visit to home page"), Url::home('http')) ?>.
      </td>
    </tr>
  </table>
</div>
<!-- END FOOTER -->

</div>
