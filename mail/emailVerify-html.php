<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
$img = $message->embed($logo);
$this->title = Yii::t('app','Account verify');
?>
      <div class="align-center">
         <img src="<?= $img ?>" width="250px">
      </div>
<div class="content">

	<!-- START CENTERED WHITE CONTAINER -->
	<table role="presentation" class="main">

		<!-- START MAIN CONTENT AREA -->
		<tr>
			<td class="wrapper">
				<table role="presentation" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<p><?php echo Yii::t('app',"Hello!") ?></p>
							<p><?php echo Yii::t('app',"This is your account activation email. Please click the button below to activate.") ?></p>
							<table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
								<tbody>
									<tr>
										<td align="left">
											<table role="presentation" border="0" cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td> 
															<a href="<?php echo $verifyLink; ?>" target="_blank"><?php echo Yii::t('app',"Activated") ?></a> 
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
								</tbody>
							</table>
							<p><?php echo Yii::t('app',"We hope you and your team will have a great performance using this web") ?></p>
							<p><?php echo Yii::t('app',"Best regards") ?>.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<!-- END MAIN CONTENT AREA -->
	</table>
	<!-- END CENTERED WHITE CONTAINER -->

	<!-- START FOOTER -->
	<div class="footer">
		<table role="presentation" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="content-block">
					<span class="apple-link"><?php echo Yii::t('app',"Source of Life Church, 636 Ngoc Lam, Long Bien, Hanoi") ?></span>
					<!-- <br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>. -->
				</td>
			</tr>
			<tr>
				<td class="content-block powered-by">
					<?= Html::a(Yii::t('app',"Visit to home page"), Url::home('http')) ?>.
				</td>
			</tr>
		</table>
	</div>
	<!-- END FOOTER -->

</div>