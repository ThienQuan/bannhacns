<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
                [
            'href' => 'dist/img/brand/favicon.png',
            'rel' => 'icon',
            'sizes' => '32x32',
        ],
        // 'dist/css/site.css',
        // 'dist/css/style.css',
        // 'dist/css/argon.css',
        // 'dist/vendor/nucleo/css/nucleo.css',
        // 'dist/vendor/@fortawesome/fontawesome-free/css/all.min.css',
        // 'dist/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
        // 'dist/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
        // 'dist/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css',
    ];
    public $js = [
        'dist/vendor/jquery/dist/jquery.min.js',
        'dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js',
        'dist/vendor/js-cookie/js.cookie.js',
        'dist/vendor/jquery.scrollbar/jquery.scrollbar.min.js',
        'dist/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js',
        'dist/vendor/datatables.net/js/jquery.dataTables.min.js',
        'dist/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
        'dist/vendor/datatables.net-buttons/js/dataTables.buttons.min.js',
        'dist/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js',
        'dist/vendor/datatables.net-buttons/js/buttons.html5.min.js',
        'dist/vendor/datatables.net-buttons/js/buttons.flash.min.js',
        'dist/vendor/datatables.net-buttons/js/buttons.print.min.js',
        'dist/vendor/datatables.net-select/js/dataTables.select.min.js',
        'dist/js/argon.js?v=1.1.0',
        'dist/js/demo.min.js',
        'dist/js/select.print.js',
        'dist/js/jspdf.min.js',
        'dist/js/print.min.js',
        'dist/js/html-chords.js',
        'dist/js/showhidechord.js',
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
