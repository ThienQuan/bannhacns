function exportPdf(title){
  var song = $('#song');  
  var cache_width = song.width();  
  var a4 =[ 595.28, 841.89];  
  createPDF();  
  function createPDF(){
    $('#show_hide').hide();
    $('#transposition').hide();
    $('.min-md-width footer').hide();
    getCanvas().then(function(canvas){  
      var  
      img = canvas.toDataURL("image/png"),  
      doc = new jsPDF({  
        unit:'px',  
        format:'a4'  
      });  
      doc.addImage(img, 'JPEG', 10, 0);
      doc.save(title+'.pdf');  
      song.width(cache_width);  
    });
    $('#show_hide').show();
    $('#transposition').show();
    $('.min-md-width footer').show();
       // create canvas object  
       function getCanvas(){  
         song.width((a4[0]*1.33333) -80).css('max-width','none');  
         return html2canvas(song,{  
          imageTimeout:2000,  
          removeContainer:true  
        });  
       }  
     }  
   }