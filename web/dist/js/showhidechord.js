

  $('#show_hide').on('click',function(){
  	if($('.chord').css('display')!='none'){
  		$('.chord').hide();
  		$('#show_hide').html('Hiện hợp âm');
  		jsHideverse();
  		jsHidenote();
  	}else{
  		$('.chord').show();
  		$('#show_hide').html('Ẩn hợp âm');
  		jsShowverse();
  		jsShownote();
  	}
  });
  function jsHideverse(){
  $("#lyric p span.verse").each(function(index){
	$(this).removeClass('font-weight-bold');
  	var s = $(this).html();
  	s = s.replace('Verse: ', "[");
	s = s.replace(s, s+']');
  		$(this).html(s);
  	});
  }
  function jsHidenote(){
  $("#lyric p span.note-lyric").each(function(index){
	$(this).removeClass('font-weight-bold');
  	var s = $(this).html();
  	s = s.replace(s, "["+s.toLowerCase()+']');
  		$(this).html(s);
  	});
  }
  function jsShowverse(){
  $("#lyric p span.verse").each(function(index){
	$(this).addClass('font-weight-bold');
  	var v = $(this).html();
	v = v.replace(/\[/g, "");
	v = v.replace(/\]/g, "");
	v = v.replace(v,'Verse: '+v);
  	// s = s.replace(/\[/g, "");
  		$(this).html(v);
  	});
  }
  function jsShownote(){
  $("#lyric p span.note-lyric").each(function(index){
	$(this).addClass('font-weight-bold');
  	var n = $(this).html();
	n = n.replace(/\[/g, "");
	n = n.replace(/\]/g, "");
	n = n.replace(n,jsUcfirst(n));
  	// s = s.replace(/\[/g, "");
  		$(this).html(n);
  	});
  }
function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function menuActive($controller)
{
    //All links begining with the controller parameter
    $selector = $('li.nav-item a[href'+'^="/'+$controller+'"]').addClass('active');
}