<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;
use app\assets\AppAsset;
use app\modules\user\models\User;
if(!Yii::$app->user->getIsGuest()){
  $checkrole = Yii::$app->user->identity->authAssignment[0]['item_name'];
}
$getid = Yii::$app->user->getId();
$user = User::find()->select('username')->where(['id' => $getid])->one();

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<?php if (Yii::$app->requestedRoute != 'site/login' && Yii::$app->requestedRoute != 'site/signup' && Yii::$app->requestedRoute != 'site/request-password-reset' && Yii::$app->requestedRoute != 'site/reset-password' && Yii::$app->requestedRoute != 'site/resend-verification-email'){ ?>
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <meta property="og:title" content="<?= Html::encode($this->title) ?>" />
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="My Heart belong to Jesus" />
    <meta property="og:url" content="http://nguonsong.xyz" />
    <meta property="og:image" content="https://nguonsong.xyz/dist/img/brand/thumbnail.png' ?>" />
    <meta property="og:image:secure_url" content="https://nguonsong.xyz/dist/img/brand/thumbnail.png' ?>" />
    <meta property="og:image:width" content="350px" />
    <meta property="og:image:height" content="200px" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <title><?= Html::encode($this->title) ?></title>
    <?php 
    $this->registerCssFile("@web/dist/css/style.css");
    $this->registerCssFile("@web/dist/css/argon.css");
    $this->registerCssFile("@web/dist/vendor/nucleo/css/nucleo.css");
    $this->registerCssFile("@web/dist/vendor/@fortawesome/fontawesome-free/css/all.min.css");
    $this->registerCssFile("@web/dist/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css");
    $this->registerCssFile("@web/dist/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css");
    $this->registerCssFile("@web/dist/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css");
    ?>
    <?php $this->head() ?>
  </head>
  <body>
    <?php $this->beginBody() ?>
    <!-- Sidenav -->
    <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
      <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header d-flex align-items-center">
          <a class="navbar-brand" href="/">
            <img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" class="navbar-brand-img" alt="...">
          </a>
          <div class="ml-auto">
            <!-- Sidenav toggler -->
            <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
              <div class="sidenav-toggler-inner">
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="navbar-inner">
          <!-- Collapse -->
          <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <h6 class="navbar-heading p-0 text-muted"><?php echo Yii::t('app','Dashboard') ?></h6>
            <?php
            echo Menu::widget([
              'items' => [
                [
                  'label' => Yii::t('app','List of songs'), 
                  'url' => Yii::$app->urlManager->createUrl('site/index'),
                  'options'=>['class'=>'nav-item'],
                  'template' => '<a href="{url}" class="nav-link"><i class="ni ni-single-copy-04 text-pink"></i><span class="nav-link-text">{label}</span></a>',
                ],
                [
                  'label' => Yii::t('app','Song for Sunday'), 
                  'url' => '#',
                  'options'=>['class'=>'nav-item'],
                  'template' => '<a href="{url}" class="nav-link"><i class="ni ni-bullet-list-67 text-orange"></i><span class="nav-link-text">{label}</span></a>',
                  'visible' => Yii::$app->user->getId()==1,
                ],
              ],
              'options' => [
                'class' => 'navbar-nav',
              ],
              'activateItems'=>function($item){
                if(isItemActive($item)==true){
                  return true;
                }else{
                  return false;
                }
              },
            ]);
            ?>
          </div>
        </div>
      </div>
    </nav>
    <!-- Main content -->
    <div class="main-content" id="panel">
      <!-- Topnav -->
      <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
        <div class="container-fluid">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Search form -->
            <?php if(!Yii::$app->user->getIsGuest()){ ?>
              <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
                <div class="form-group mb-0">
                  <div class="input-group input-group-alternative input-group-merge">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fas fa-search"></i></span>
                    </div>
                    <input class="form-control" placeholder="<?php echo Yii::t('app','Find song') ?>" type="text">
                  </div>
                </div>
                <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </form>
            <?php } ?>
            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center ml-md-auto">
              <?php if(!Yii::$app->user->getIsGuest()){ ?>
                <li class="nav-item d-xl-none">
                  <!-- Sidenav toggler -->
                  <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                      <i class="sidenav-toggler-line"></i>
                    </div>
                  </div>
                </li>
                <li class="nav-item d-sm-none">
                  <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                    <i class="ni ni-zoom-split-in"></i>
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="ni ni-bell-55"></i>
                  </a>
                  <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right py-0 overflow-hidden">
                    <!-- Dropdown header -->
                    <div class="px-3 py-3">
                      <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">1</strong> notifications.</h6>
                    </div>
                    <!-- List group -->
                    <div class="list-group list-group-flush">
                      <a href="#!" class="list-group-item list-group-item-action">
                        <div class="row align-items-center">
                          <div class="col-auto">
                            <!-- Avatar -->
                            <img alt="Image placeholder" src="<?php echo Yii::getAlias('@web').'/dist/img/brand/favicon.png' ?>" class="avatar rounded-circle">
                          </div>
                          <div class="col ml--2">
                            <div class="d-flex justify-content-between align-items-center">
                              <div>
                                <h4 class="mb-0 text-sm">John Snow</h4>
                              </div>
                              <div class="text-right text-muted">
                                <small>2 hrs ago</small>
                              </div>
                            </div>
                            <p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
                          </div>
                        </div>
                      </a>
                    </div>
                    <!-- View all -->
                    <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
                  </div>
                </li>
              <?php } ?>
            </ul>
            <ul class="navbar-nav align-items-center ml-auto ml-md-0">
              <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <div class="media align-items-center">
                    <?php if(!Yii::$app->user->getIsGuest()){ ?>
                      <span class="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="https://i.imgur.com/D4O9mlj.jpg' ?>" style="width: 36px;height: 36px;object-fit: cover;">
                      </span>
                      <div class="media-body ml-2 d-none d-lg-block">
                        
                        <span class="mb-0 text-sm  font-weight-bold" style="text-transform: capitalize;"><?php  echo $user->username; ?></span>
                      </div>
                    <?php }else{ ?>
                      <div class="media-body ml-2 d-none d-lg-block">
                        <span class="mb-0 text-sm  font-weight-bold">Welcome Guest</span>
                      </div>
                    <?php } ?>
                  </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                  <?php if(!Yii::$app->user->getIsGuest()){ ?>
                    <div class="dropdown-header noti-title">
                      <h6 class="text-overflow m-0">Welcome!</h6>
                    </div>
                    <?php if($checkrole == 'admin'){ ?>
                      <a href="#!" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span><?php echo Yii::t('app','Personal information') ?></span>
                      </a>
                      <a href="#!" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span><?php echo Yii::t('app','Setting') ?></span>
                      </a>
                      <a href="#!" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span><?php echo Yii::t('app','Support') ?></span>
                      </a>
                    <?php } ?>
                    <div class="dropdown-divider"></div>
                    <?php echo Html::beginForm(['/site/logout'], 'post',['class'=>'dropdown-item']). Html::submitButton(Yii::t('app','<i class="ni ni-user-run"></i>'. Yii::t('app','Logout')),['class' => 'dropdown-item px-0']). Html::endForm()?>
                  <?php }else{ ?>
                    <a href="<?php echo Yii::$app->urlManager->createUrl('site/login'); ?>" class="dropdown-item">
                      <i class="ni ni-support-16"></i>
                      <span><?php echo Yii::t('app','Login') ?></span>
                    </a>
                  <?php } ?>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- Header -->
      <?php if (Yii::$app->requestedRoute == 'song/default/create'){ ?>

        <div class="d-none">
        <?php } ?>
        <?= $content ?>
        <!-- Footer -->
        <footer class="footer pt-0">
          <div class="row align-items-center justify-content-lg-between w-100">
            <div class="col-lg-6">
              <div class="copyright text-center text-lg-left text-muted">
                &copy; 2020 <a href="#" class="font-weight-bold ml-1" target="_blank">Nguon Song Band</a>
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <?php $this->endBody() ?>
  </body>
<!--     <?php }elseif (Yii::$app->requestedRoute == 'site/check'){ ?>
  <?= $content ?> -->
<?php }else{ ?>
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
    $this->registerCssFile("@web/dist/vendor/fontawesome-free/css/all.min.css");
    $this->registerCssFile("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css");
    $this->registerCssFile("@web/dist/vendor/icheck-bootstrap/icheck-bootstrap.min.css");
    $this->registerCssFile("@web/dist/css/adminlte.min.css"); ?>
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta property="og:title" content="<?= Html::encode($this->title) ?>" />
    <meta property="og:type" content="article"/>
    <meta property="og:site_name" content="My Heart belong to Jesus" />
    <meta property="og:url" content="http://nguonsong.xyz/site/login" />
    <meta property="og:image" content="https://nguonsong.xyz/dist/img/brand/thumbnail.png' ?>" />
    <meta property="og:image:secure_url" content="https://nguonsong.xyz/dist/img/brand/thumbnail.png' ?>" />
    <meta property="og:image:width" content="350px" />
    <meta property="og:image:height" content="200px" />
    <?php $this->head() ?>
  </head>
  <?php $this->beginBody() ?>
  <?= $content ?>
  <?php $this->endBody() ?>
<?php } ?>
</html>
<?php $this->endPage() ?>
