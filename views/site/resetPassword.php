<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<body class="hold-transition login-page">
    <div class="background-color">
    </div>
    <div class="login-box">
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body m-4">
            <div class="login-logo">
                <a href="#"><img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" style="width:180px;"></a>
            </div>
            <p class="login-box-msg"><?php echo Yii::t('app','You are only one step away from your new password, recover your password now') ?>.</p>
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <form>
                     <?= $form->field($model, 'password',[
                   'template' => '
               <div class="input-group mb-3">
                   {input}
                   <div class="input-group-append">
                   <div class="input-group-text">
                   <span class="fas fa-lock"></span>
                   </div>
                   </div>
                   </div>
                   <div class="text-danger">{error}</div>'
               ])->passwordInput(['class'=>"form-control",'id'=>'inputPassword','placeholder'=>Yii::t("app","New password")])->label(false) ?>
            <div class="row">
              <div class="col-12">
                <?= Html::submitButton(Yii::t("app","Change password"), ['class' => 'btn btn-primary btn-block', 'name' => 'request-button']) ?>
              </div>
            </div>

        </form>
        <?php ActiveForm::end(); ?>
        <p class="mt-3 mb-1">
        <a href="<?php echo Yii::$app->urlManager->createUrl('site/login'); ?>"><?php echo Yii::t('app',"Login") ?></a>
      </p>
        <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
</div>
</div>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/js/adminlte.min.js' ?>"></script>
</body>