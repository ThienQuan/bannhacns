<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app','Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<body class="hold-transition register-page sidebar-mini">
    <div class="register-box">
      <div class="card">
        <div class="card-body register-card-body">
            <div class="login-logo">
                <a href="#"><img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" style="width:180px;"></a>
            </div>
            <p class="login-box-msg"><?php echo Yii::t('app','Register a new membership') ?></p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <form>
                <?= $form->field($model, 'username',[
                 'template' => '
                 <div class="input-group mb-3">
                 {input}
                 <div class="input-group-append">
                 <div class="input-group-text">
                 <span class="fas fa-user"></span>
                 </div>
                 </div></div> <div class="text-danger">{error}</div>'
             ])->textInput(['class'=>"form-control",'id'=>'inputUserName','placeholder'=>'User Name'])->label(false) ?>

                <?= $form->field($model, 'email',[
                   'template' => '<div class="input-group mb-3">
                   {input}
                   <div class="input-group-append">
                   <div class="input-group-text">
                   <span class="fas fa-envelope"></span>
                   </div></div>
                   </div>
                   <div class="text-danger">{error}</div>'
               ])->Input('email', ['class'=>"form-control",'id'=>'inputemail','placeholder'=>'Email']) ?>
                <?= $form->field($model, 'password',[
                 'template' => '
                 <div class="input-group mb-3">
                 {input}
                 <div class="input-group-append">
                 <div class="input-group-text">
                 <span class="fas fa-lock"></span>
                 </div>
                 </div>
                 </div>
                 <div class="text-danger">{error}</div>'
             ])->passwordInput(['class'=>"form-control",'id'=>'inputPassword','placeholder'=>'Password'])->label(false) ?>
             <div class="row">
              <!--<div class="col-8">
                <div class="icheck-primary">
                  <input type="checkbox" id="agreeTerms" name="terms" value="agree">
                  <label for="agreeTerms">
                     I agree to the <a href="#">terms</a>
                 </label>
             </div></div>-->
<!-- /.col -->
<div class="col-4 offset-8">
    <?= Html::submitButton(Yii::t('app','Register'), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
</div>
<!-- /.col -->
</div>
</form>
<?php ActiveForm::end(); ?>
<div class="social-auth-links text-center">
    <p>- OR -</p>
    <a href="#" class="btn btn-block btn-primary" data-toggle="modal" data-target="#modal">
      <i class="fab fa-facebook mr-2"></i>
      Sign up using Facebook
  </a>
  <a href="#" class="btn btn-block btn-danger" data-toggle="modal" data-target="#modal">
      <i class="fab fa-google-plus mr-2"></i>
      Sign up using Google+
  </a>
<div class="modal fade" id="modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="text-center"><?php echo Yii::t('app','Notification') ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?php echo Yii::t('app','Updating, please come back later') ?>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo Yii::t('app','Back') ?></button>
            </div>
        </div>
    </div>
</div>
</div>
        <p class="mt-3 mb-1">
<a href="<?php echo Yii::$app->urlManager->createUrl('site/login'); ?>" class="text-center"><?php echo Yii::t('app','I already have a membership') ?></a>
</p>
      <p class="mb-0">
<a href="<?php echo Yii::$app->urlManager->createUrl('site/resend-verification-email'); ?>" class="text-center"><?php echo Yii::t('app','Resend Verification Email') ?></a>
</p>
</div>
</div>
</div>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/js/adminlte.min.js' ?>"></script>
</body>