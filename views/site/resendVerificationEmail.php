<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Resend verification email';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile("@web/dist/vendor/fontawesome-free/css/all.min.css");
$this->registerCssFile("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css");
$this->registerCssFile("@web/dist/vendor/icheck-bootstrap/icheck-bootstrap.min.css");
$this->registerCssFile("@web/dist/css/adminlte.min.css");

$this->registerCss("body *{font-family: Open Sans,sans-serif;}");
?>
<body class="hold-transition login-page">
    <div class="background-color">
    </div>
    <div class="login-box">
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body m-4">
            <div class="login-logo">
                <a href="#"><img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" style="width:180px;"></a>
            </div>
            <p class="login-box-msg"><?php echo Yii::t('app','Please fill out your email. A link to reset password will be sent there') ?>.</p>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <form>
                     <?= $form->field($model, 'email',[
                   'template' => '<div class="input-group mb-3">
                   {input}
                   <div class="input-group-append">
                   <div class="input-group-text">
                   <span class="fas fa-envelope"></span>
                   </div></div>
                   </div>
                   <div class="text-danger">{error}</div>'
               ])->Input('email', ['class'=>"form-control",'id'=>'inputemail','placeholder'=>'Email']) ?>
            <div class="row">
              <div class="col-12">
                <?= Html::submitButton(Yii::t("app","Request resend verification email"), ['class' => 'btn btn-primary btn-block', 'name' => 'request-button']) ?>
              </div>
            </div>

        </form>
        <?php ActiveForm::end(); ?>
        <p class="mt-3 mb-1">
        <a href="<?php echo Yii::$app->urlManager->createUrl('site/login'); ?>"><?php echo Yii::t('app',"Login") ?></a>
      </p>
      <p class="mb-0">
        <a href="<?php echo Yii::$app->urlManager->createUrl('site/signup'); ?>" class="text-center"><?php echo Yii::t('app',"Register a new membership") ?></a>
      </p>
        <!-- /.social-auth-links -->

    </div>
    <!-- /.login-card-body -->
</div>
</div>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/js/adminlte.min.js' ?>"></script>
</body>