<?php

/* @var $this yii\web\View */
use yii\helpers\ArrayHelper;
use app\modules\type\models\Type;
use app\modules\topic\models\Topic;
use app\modules\song\models\Song;
use app\modules\user\models\User;
use yii\helpers\Url;

$getusernumber = new User;
// echo "<pre>";print_r($this);die;
$this->title = Yii::t('app','List of songs');

// $get_arr_type = ArrayHelper::map(type::find()->all(),'id','type_name');
$get_arr_song = song::find()->select(['id','title','first_lyric','chorus','type_id','key_chord'])->orderBy(['id'=>SORT_DESC])->all();
// echo "<pre>";print_r($get_arr_song);die;
$i = 1;
$getuser = $getusernumber->getusernumber();
$demo = 2;
// echo Yii::$app->user->getId();die;
?>
<!-- Header -->
<div class="header bg-primary pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0"><?php echo Yii::t('app','List of songs') ?></h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item"><a href="<?php echo Yii::$app->urlManager->createUrl('site/index'); ?>"><?php echo Yii::t('app','List of songs') ?></a></li>
            </ol>
          </nav>
        </div>
        <?php if(!Yii::$app->user->getIsGuest()){ ?>
        <div class="col-lg-6 col-5 text-right">
          <?php if(Yii::$app->user->getId()!=$getuser){ ?>
          <a href="<?php echo Yii::$app->urlManager->createUrl('song/default/create'); ?>" class="btn btn-sm btn-neutral"><i class="ni ni-fat-add mr-0"></i> <span class="d-none d-md-inline"><?php echo Yii::t('app','Create song') ?></span></a>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
  <div class="row">
    <div class="col">
      <div class="card">
        <!-- Card header -->
        <div class="card-header">
          <h3 class="mb-0"><?php echo Yii::t('app','List of songs') ?></h3>
        </div>
        <div class="table-responsive py-4">
          <table class="table table-flush" id="datatable-basic">
            <thead class="thead-light">
              <tr>
                <th>#</th>
                <th><?php echo Yii::t('app','Title song') ?></th>
                <th><?php echo Yii::t('app','First Verse') ?></th>
                <th><?php echo Yii::t('app','First Chorus') ?></th>
                <th><?php echo Yii::t('app','Key song') ?></th>
                <th><?php echo Yii::t('app','Type') ?></th>
                <th></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>#</th>
                <th><?php echo Yii::t('app','Title song') ?></th>
                <th><?php echo Yii::t('app','First Verse') ?></th>
                <th><?php echo Yii::t('app','First Chorus') ?></th>
                <th><?php echo Yii::t('app','Key song') ?></th>
                <th><?php echo Yii::t('app','Type') ?></th>
                <th></th>
              </tr>
            </tfoot>
            <tbody>
                <?php foreach ($get_arr_song as $song) { 

                // echo "<pre>";print_r($song['title']);die;
                    // echo $song['title'];die;
                ?>
              <tr>
                <td><?php echo $i ?></td>
                <td>
                  <a href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>" class="font-weight-bold"><?php echo $song['title']; ?></a>
                </td>
                <td>
                  <a href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>"><?php echo $song['first_lyric']; ?>...</a>
                </td>
                <td>
                  <a href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>"><?php echo $song['chorus']; ?>...</a>
                </td>
                <td>
                  <a href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>"><?php echo $song['key_chord']; ?></a>
                </td>
                <td>
                  <a href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>"><?php $type = type::find()->select('type_name')->where(['id' => $song['type_id']])->one();
                  if(empty($type['type_name'])){
                    echo "<span style='color:red;'>Chưa cập nhật</span>";
                  }else{
                      echo $type['type_name']; 
                  }
                  ?></a>

                </td>
                <td class="text-right">
                  <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                      <a class="dropdown-item" href="<?php echo Url::to(['song/default/view', 'id' => $song['id']]); ?>">Chi tiết</a>
                      <a class="dropdown-item" href="<?php echo Url::to(['song/default/update', 'id' => $song['id']]); ?>">Sửa</a>
                      <a class="dropdown-item" href="<?php echo Url::to(['song/default/delete', 'id' => $song['id']]); ?>">Xóa</a>
                    </div>
                  </div>
                </td>
              </tr>
                <?php $i++;
                 } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>