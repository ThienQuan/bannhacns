<?php 
use yii\web\View;

$this->registerCssFile("@web/dist/vendor/fontawesome-free/css/all.min.css");
$this->registerCssFile("https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css");
$this->registerCssFile("@web/dist/vendor/icheck-bootstrap/icheck-bootstrap.min.css");
$this->registerCssFile("@web/dist/vendor/icheck-bootstrap/icheck-bootstrap.min.css");
$this->registerCssFile("@web/dist/css/adminlte.min.css");
$this->registerCssFile("https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700");
 ?>
<body class="hold-transition login-page">
<div class="background-color">
</div>
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body m-4">
		<div class="login-logo">
		<a href="../../index3.html"><img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" style="width:180px;"></a>
	  </div>
      <p class="login-box-msg">Sign in to start your session</p>

      <form action="../../index3.html" method="post">
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-6">
			<p style="text-align:right;">
				<a href="forgot-password.html">Forgot Password?</a>
			  </p>
            
          </div>
          <!-- /.col -->
        </div>
		<button type="submit" class="btn btn-primary btn-block">Sign In</button>
      </form>

      <!-- /.social-auth-links -->

     
	  <p class="mb-1 mt-4" style="text-align:center;">
        Don't have account ?<a href="register.html"> Sign Up Here</a>
      </p>

    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/js/adminlte.min.js' ?>"></script>
</body>