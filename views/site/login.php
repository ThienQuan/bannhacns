<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = Yii::t('app','Login');
$this->params['breadcrumbs'][] = $this->title;
$test = 1;
if($test==1){
    $placeholder_user = 'demo';
    $placeholder_pass = 'demo123';
}else{
    $placeholder_user = 'Enter Use Name';
    $placeholder_pass = 'Enter Use Name';
}
$rememberMe = Yii::t('app','Remember Me');
$forgot = Yii::t('app','Forgot Password?');
$this->registerCss("body *{font-family: Open Sans,sans-serif;}#remember-me::after { content: '$rememberMe'; } #forgot a::before{content:'$forgot';}");
?>
<body class="hold-transition login-page">
    <div class="background-color">
    </div>
    <div class="login-box">
      <!-- /.login-logo -->
      <div class="card">
        <div class="card-body login-card-body m-4">
            <div class="login-logo">
                <a href="#"><img src="<?php echo Yii::getAlias('@web').'/dist/img/brand/Logo_app.png' ?>" style="width:180px;"></a>
            </div>
            <p class="login-box-msg"><?php echo Yii::t('app','Sign in to start your session') ?></p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <form>
                    <?= $form->field($model, 'username',[
                       'template' => '
                        <div class="input-group mb-3">
                       {input}
                       <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-user"></span>
                          </div>
                       </div></div> <div class="text-danger">{error}</div>'
                   ])->textInput(['class'=>"form-control",'id'=>'inputUserName','placeholder'=>$placeholder_user])->label(false) ?>
                <?= $form->field($model, 'password',[
                   'template' => '
               <div class="input-group mb-3">
                   {input}
                   <div class="input-group-append">
                   <div class="input-group-text">
                   <span class="fas fa-lock"></span>
                   </div>
                   </div>
                   </div>
                   <div class="text-danger">{error}</div>'
               ])->passwordInput(['class'=>"form-control",'id'=>'inputPassword','placeholder'=>$placeholder_pass])->label(false) ?>
            <?= $form->field($model, 'rememberMe', ['template'=>'
           <div class="row">
                <div class="col-6">
                    <div class="icheck-primary">
                        {input}
                        <label for="loginform-rememberme"><span id="remember-me"></span>
                        </label>
                    </div>
                </div>
                <div class="col-6">
                    <p style="text-align:right;margin-top:4px;" id="forgot">
                        <a href="#"><?php echo Yii::t("app","Forgot Password?") ?></a>
                    </p>
                </div>'])->textInput(['class'=>"custom-control-input",'type'=>'checkbox'])?>
                <!-- /.col -->
                
                <!-- /.col -->
            </div>
            <?= Html::submitButton(Yii::t("app","Sign In"), ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </form>
        <?php ActiveForm::end(); ?>

        <!-- /.social-auth-links -->


        <p class="mb-1 mt-4 d-none" style="text-align:center;">
            <?php echo Yii::t('app',"Don't have account?") ?><a href="<?php echo Yii::$app->urlManager->createUrl('site/signup'); ?>"> <?php echo Yii::t('app',"Sign Up Here") ?></a>
        </p>

    </div>
    <!-- /.login-card-body -->
</div>
</div>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/jquery/dist/jquery.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/vendor/bootstrap/dist/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?php echo Yii::getAlias('@web').'/dist/js/adminlte.min.js' ?>"></script>
<script>
    $( document ).ready(function() {
        $('#forgot a').attr("href", "<?php echo Yii::$app->urlManager->createUrl('site/request-password-reset'); ?>");
    });
</script>
</body>