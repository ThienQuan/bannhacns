<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;
use yii\helpers\Url;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\app\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => Yii::t('app','There is no user with this email address.')
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }
        // $img = Yii::getAlias('@web').'/dist/img/brand/Logo_app.png';
        $message = Yii::$app->mailer->compose(
                'passwordResetToken-html',
                [
                    'user' => $user, 
                    'logo' => Yii::getAlias('@webroot').'/dist/img/brand/Logo_app.png'
                ]
            );
        $message->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->name . ' robot']);
        $message->setTo($this->email);
        $message->setSubject(Yii::t('app',"Password reset for account").' ' . $user->username);
        $message->send();
        return $message;
    }
}
